const fs = require('fs')

const queries = require('./queries')

console.log(queries)

const cassandra = require('cassandra-driver')

function LoadOptions(argv) {
    // Read in the secrets file.
    if (argv.length < 3) {
        console.log('LoadOptions', 'Argument 2 must be an options file name.')
        process.exit(1)
    } else {
        const optionsFname = argv[2]
        const fileText = fs.readFileSync(optionsFname)
        return JSON.parse(fileText)
    }
}

async function Main(options) {
	console.log('Main', {options})
	const client = new cassandra.Client(options.cassandra)
	const connectResult = await client.connect() 
	console.log('Main', {connectResult})

	for (const query of queries) {
		console.log('Main', {query})
		const result = await client.execute(query)
		console.log('Main', {result})
	}

	process.exit(0)
}

Main(LoadOptions(process.argv))

