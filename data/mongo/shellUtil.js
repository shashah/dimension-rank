const fs = require('fs')

const debug = false

const optionsFname = process.argv[2]
if (!optionsFname) {
    console.log('No file name given')
    process.exit(1)
}

const fileText = fs.readFileSync(optionsFname)
const json = JSON.parse(fileText)
if (debug)
console.log({ json })

const uri = json.mongoUri

if (debug)
console.log({ uri })


const questionParts = uri.split('?')
if (debug) console.log({ questionParts })

const mainUriPart = questionParts[0]

const atParts = mainUriPart.split('@')

const bloatedAuthPart = atParts[0]
const clientUrlPart = atParts[1]


if (debug) console.log({ bloatedAuthPart })
if (debug) console.log({ clientUrlPart })

const authPart = bloatedAuthPart.split('//')[1]
if (debug) console.log({ authPart })

const authParts = authPart.split(':')

const userName = authParts[0]
const password = authParts[1]


const finalCommand = 'mongo mongodb+srv://' + clientUrlPart + ' --username ' + userName + ' -p ' + password


console.log(finalCommand)
