const redis = require('redis')
const rfr = require('rfr')

const config = rfr('data/config')

var globalClient = null

function client() {
	return globalClient
}

async function ResetRedisClient(options) {
	console.log('ResetRedisClient', {options})
	globalClient = redis.createClient(options)
	globalClient.on("error", function(error) {
			console.error('globalClient', error)
	})
	return undefined
}

module.exports = {
    ResetRedisClient,
		client,
}
