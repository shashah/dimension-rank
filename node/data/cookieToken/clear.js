const rfr = require('rfr')
const dataRedis = rfr('data/redis')

async function ClearCookieToken(cookieToken) {
    console.log('ClearCookieToken', 'cookieToken', cookieToken)
    return new Promise(function(resolve, reject) {
        console.log('ClearCookieToken', 'promise')
        dataRedis.client().hdel("mapCookieToken", cookieToken, function(error, result) {
            console.log('ClearCookieToken', 'error', error)
            if (error) {
                reject(error)
            } else {
                resolve(result)
            }
        })
    })
}

module.exports = {
    ClearCookieToken,
}
