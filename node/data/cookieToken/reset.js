// This file contains the *utility* functions for accessing the in-memory
// Redis table 'mapCookieToken'.
//
// This is NOT the authentication middleware. This will be called by the
// middleware.
const rfr = require('rfr')
const dataRedis = rfr('data/redis')

function RandomToken(length) {
    var result = '';
    var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}

function RedactUserInfo(user) {
    console.log('RedactUserInfo', {user})
    return {
        _id: user._id.toString(),
        userName: user.userName,
    }
}

// Returns a Promise.
async function ResetCookieToken(user) {
    console.log('ResetCookieToken', {user})
    const cookieToken = RandomToken(24)
    console.log('ResetCookieToken', {cookieToken})
    const redactedUser = RedactUserInfo(user)
    console.log('ResetCookieToken', {redactedUser})
    const jsonUser = JSON.stringify(redactedUser)
    console.log('ResetCookieToken', {jsonUser})

    return new Promise(function(resolve, reject) {
    console.log('ResetCookieToken', 'promise')
        dataRedis.client().hset('mapCookieToken', cookieToken, jsonUser, err => {
    console.log('ResetCookieToken', {err})
            if (err) {
                reject(err)
            } else {
                resolve(cookieToken)
            }
        })
    })
}

module.exports = {
    ResetCookieToken,
}
