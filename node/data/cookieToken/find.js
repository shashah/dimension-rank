const rfr = require('rfr')
const dataRedis = rfr('data/redis')

async function FindInternal(params, output) {
    dataRedis.client().hget("mapCookieToken", params.cookieToken, function(error, userRep) {
        console.log('FindCookieTokenUser', 'hget', error, userRep)
				if (userRep) {
						output.resolve(JSON.parse(userRep))
				} else {
						output.reject({
								loginFailed: true,
						})
				}
    })
}

async function FindCookieTokenUser(cookieToken) {
    console.log('FindCookieTokenUser', {
        cookieToken
    })
    return new Promise(function(resolve, reject) {
			FindInternal({cookieToken}, {resolve, reject})
		})
}

module.exports = {
    FindCookieTokenUser,
}
