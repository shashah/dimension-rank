const mongoose = require('mongoose')
const rfr = require('rfr')

const objects = require('data/objects')

async function ActuallyCreateTheUser(params, output) {
    console.log('ActuallyCreateTheUser', 'params', params)
    const user = objects.User({
        _id: new mongoose.Types.ObjectId(),
        email: params.email,
        userName: params.userName,
        description: params.description,
        profile: params.profile,
    })
    user.save()
        .then(function(result) {
            output.resolve(result)
        })
        .catch(error => {
            output.reject(error)
        })
}

async function CheckIfUserNameTaken(params, output) {
    console.log('CheckIfUserNameTaken', 'params', params)
    objects.User.findOne({
            userName: params.userName,
        })
        .then(result => {
            console.log('CheckIfUserNameTaken', 'result', result)
            if (result) {
                output.reject({
                    error: 'userNameAlreadyTaken',
                })
            } else {
                ActuallyCreateTheUser(params, output)
            }
        })
        .catch(error => {
            output.resolve(error)
        })
}

async function CheckIfEmailHasAccount(params, output) {
    objects.User.findOne({
            email: params.email,
        })
        .then(user => {
            console.log('CheckIfEmailHasAccount', 'user', user)
            if (user) {
                output.resolve({
                    error: 'emailHasAccount',
                })
            } else {
                CheckIfUserNameTaken(params, output)
            }
        })
        .catch(error => {
            output.resolve(error)
        })
}

async function CheckIfEmailAuthorized(params, output) {
    objects.EmailAuthorization.findOne({
            email: params.email
        })
        .then(authorization => {
            if (authorization) {
                CheckIfEmailHasAccount(params, output)
            } else {
                output.resolve({
                    error: 'emailNotAuthorized',
                })
            }
        })
        .catch(error => {
            output.resolve(error)
        })
}

// Will create a user if:
//   1) 'email' is authorized
//   2) 'email' does not yet have an account
//   3) 'userName' is not taken
async function TryCreateUser(userName, email) {
    return new Promise(function(
        resolve,
        reject) {
        CheckIfEmailAuthorized({
            userName,
            email
        }, {
            resolve,
            reject
        })
    })
}

module.exports = {
    TryCreateUser,
}
