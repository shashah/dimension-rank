const mongoose = require('mongoose')
const rfr = require('rfr')

const objects = rfr('data/objects')

// Returns a promise.
async function CheckEmailAuthorization(email) {
	return objects.EmailAuthorization.findOne( { email } )
}

module.exports = {
    CheckEmailAuthorization,
}
