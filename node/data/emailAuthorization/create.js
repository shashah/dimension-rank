const mongoose = require('mongoose')
const rfr = require('rfr')

const objects = rfr('data/objects')

// Returns a promise.
async function CreateEmailAuthorization(email) {
    console.log('CreateEmailAuthorization', {email})
    const emailApproval = new objects.EmailAuthorization({
        _id: new mongoose.Types.ObjectId(),
        email,
    })

    return emailApproval.save()
}

module.exports = {
    CreateEmailAuthorization,
}
