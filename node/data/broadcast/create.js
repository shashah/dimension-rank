const mongoose = require('mongoose')
const getUrls = require('get-urls')
const rfr = require('rfr')

const dataRedis = rfr('data/redis')
const objects = rfr('data/objects')
const getResource = rfr('data/resource/get')
const addGenericHot = rfr('data/genericHot/add')
const referenceBroadcast = rfr('data/broadcast/reference')
const config = rfr('data/config')

async function AddBroadcastToUnsorted(params, output) {
    console.log('AddBroadcastToUnsorted', {
        params
    })
    const broadcast = params.broadcast
    console.log('AddBroadcastToUnsorted', {
        broadcast
    })
    const broadcastString = JSON.stringify(broadcast)
    console.log('AddBroadcastToUnsorted', {
        broadcastString
    })
    return new Promise(function(resolve, reject) {
        dataRedis.client().rpush('queueUnsortedBroadcasts', broadcastString, function(error, result) {
            if (error) {
                output.reject(error)
            } else {
                output.resolve(undefined)
            }
        })
    })
}

async function AddBroadcastToQueues(params, output) {
    return new Promise(function(resolve, reject) {
        AddBroadcastToUnsorted(
            params, {
                resolve,
                reject
            })
    })
}

async function SaveBroadcast(params, output) {
    console.log('SaveBroadcast', {
        params
    })
    const creationTime = 0
    const broadcast = new objects.Broadcast({
        _id: new mongoose.Types.ObjectId(),
        textarea: params.textarea,
        channelDescriptor: params.channelDescriptor,
        tokens: params.tokens,
        resources: params.resources,
        producerTuple: params.producerTuple,
        creationTime,
        reference: params.reference,
    })
    // console.log('SaveBroadcast', 'broadcast', broadcast)

    broadcast.save().then(broadcastResult => {
        output.resolve(broadcastResult)
        AddBroadcastToQueues({
            ...params,
            broadcast
        }, output).then(success => {
            // console.log('SaveBroadcast', 'success', success)
        }).catch(error => {
            // console.log('SaveBroadcast', 'error', error)
        })
    }).catch(error => {
        output.reject(error)
    })
}

// Loops through each element of 'tokens'.
// If it is a url, look this up in the database, and add the value to a resource.
// @async returns a Promise; has to wait for the network.
async function MaybeSerializeResources(params, output) {
    console.log('MaybeSerializeResources', {
        params
    })
    var promiseBuffer = []
    for (const token of params.tokens) {
        // console.log('MaybeSerializeResources', 'token.isUrl', token.isUrl)
        if (token.isUrl) {
            // Note: we have checked that 'token' is a url
            const promise = getResource.BlockingResouceFromUrl(token.token)
            promiseBuffer.push(promise)
        }
    }

    try {
        var resourceBuffer = []
        // console.log('MaybeSerializeResources', 'promiseBuffer.length', promiseBuffer.length)
        for (var promise of promiseBuffer) {
            // Note: await
            const resource = await promise
            // console.log('MaybeSerializeResources', 'resource', resource)
            resourceBuffer.push(resource)
        }
    } catch (error) {
        output.reject(error)
    }

    params.resources = resourceBuffer
    SaveBroadcast(params, output)
}

function TokenizeTextarea(textarea) {
    var buffer = []
    const parts = textarea.split(/(\s+)/)
    for (const part of parts) {
        const token = part.trim()
        if (token.length > 0) {
            buffer.push(token)
        }
    }
    return buffer
}


async function MaybeSaveReference(params, output) {
    console.log('MaybeSaveReference', {
        params
    })
    if (params.referenceId && params.polarity) {
        console.log('MaybeSaveReference', {
            params
        })
        referenceBroadcast.ReferenceFromId(params.referenceId, params.polarity).then(referenceResult => {
            console.log('MaybeSaveReference', {
                referenceResult
            })

            MaybeSerializeResources({
                ...params,
                reference: referenceResult,
            }, output)
        }).catch(error => {
            output.reject(error)
        })
    } else {
        MaybeSerializeResources({
            ...params,
            reference: null,
        }, output)
    }

}

// TODO(greg) Consolidate this with other versions.
function SanitizeTuple(fatTuple) {
    return {
        _id: fatTuple._id,
        userName: fatTuple.userName,
    }
}

// producerTuple has elements {'_id', 'userName'}
async function CreateBroadcast(textarea, channelDescriptor, producerTuple, referenceId, polarity) {
    return new Promise(function(resolve, reject) {
        const tokens = TokenizeTextarea(textarea)
        MaybeSaveReference({
            textarea,
						channelDescriptor,
            producerTuple: SanitizeTuple(producerTuple),
            tokens,
            referenceId,
            polarity,
        }, {
            resolve,
            reject,
        })
    })
}

module.exports = {
    CreateBroadcast,
}
