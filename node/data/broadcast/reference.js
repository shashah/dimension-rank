const rfr = require('rfr')

const objects = rfr('data/objects')

function ReferenceFromBroadcast(broadcast, polarity) {
    console.log('ReferenceFromBroadcast', {
        broadcast
    }, {
        polarity
    })
    return {
        _id: broadcast._id.toString(),
        tokens: broadcast.tokens,
        polarity,
        resources: broadcast.resources,
        producerTuple: broadcast.producerTuple,
    }
}

async function ReadBroadcast(params, output) {
    console.log('ReadBroadcast', {
        params
    })
    const id = params.referenceId
    console.log('ReadBroadcast', {
        id
    })
    objects.Broadcast.findById(id).then(broadcast => {
        if (broadcast) {
            const reference = ReferenceFromBroadcast(broadcast, params.polarity)
            output.resolve(reference)
        } else {
            output.reject('broadcast is null')
        }
    }).catch(error => {
        output.reject(error)
    })
}

// referenceId: String
// polarity: String
async function ReferenceFromId(referenceId, polarity) {
    return new Promise(function(resolve, reject) {
        ReadBroadcast({
            referenceId,
						polarity,
        }, {
            resolve,
            reject,
        })
    })
}

module.exports = {
    ReferenceFromId,
}
