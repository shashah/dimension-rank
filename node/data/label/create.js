const mongoose = require('mongoose')
const redis = require('redis')
const rfr = require('rfr')

const objects = rfr('data/objects')
const dataRedis = rfr('data/redis')

async function SaveLabelToRam(labelResult, output) {
    console.log('SaveLabelToRam', {
        labelResult
    })
    const labelString = JSON.stringify(labelResult)
    dataRedis.client().rpush('queueBroadcastLabels', labelString, function(error, result) {
        if (error) {
            output.reject(error)
        } else {
            output.resolve(labelResult)
        }
    })
}

async function SaveLabelToDisk(params, output) {
    console.log('SaveLabelToDisk', 'labelValue', params.labelValue)
    const label = new objects.BroadcastLabel({
        _id: new mongoose.Types.ObjectId(),
        subjectId: params.subjectId,
        objectId: params.broadcastId,
        authorId: params.broadcast.producerTuple._id,
        labelName: params.labelName,
        labelValue: params.labelValue,
        creationTime: 0,
    })
    label.save()
        .then(labelResult => {
            SaveLabelToRam(labelResult, output)
        })
        .catch(error => {
            output.reject(error)
        })
}

// 'params' is the set of arguments put into a dictionary.
async function LookupBroadcast(params, output) {
    console.log('LookupBroadcast', {
        params
    })
    objects.Broadcast.findById(params.broadcastId)
        .then(broadcast => {
            console.log('LookupBroadcast', {
                broadcast
            })
            if (broadcast) {
                SaveLabelToDisk({
                    ...params,
                    broadcast
                }, output)
            } else {
                output.reject({
                    error: 'cannotFindBroadcast'
                })
            }
        })
        .catch(error => {
            console.log('LookupBroadcast', {
                error
            })
            output.reject(error)
        })
}

// producerTuple has elements {'_id', 'userName'}
async function LabelBroadcast(subjectId, broadcastId, objectType, labelName, labelValue) {
    return new Promise(function(resolve, reject) {
        LookupBroadcast({
            subjectId,
            broadcastId,
            objectType,
            labelName,
            labelValue
        }, {
            resolve,
            reject
        })
    })
}

module.exports = {
    LabelBroadcast,
}
