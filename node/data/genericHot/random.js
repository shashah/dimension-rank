const redis = require('redis')
const rfr = require('rfr')

const objects = rfr('data/objects')
const dataRedis = rfr('data/redis')

// Note: We are adding the *full* document.
async function AddDocument(broadcast) {
    const jsonString = JSON.stringify(broadcast)
    return new Promise(function(resolve, reject) {
        if (!broadcast) {
            reject({
                error: 'badArgument',
                detail: 'documentId',
            })
        }
        dataRedis.client().sadd("setGeneralHotDocuments", jsonString, function(error, result) {
            if (error) {
                reject(error)
            } else {
                resolve(result)
            }
        })
    })
}

async function PickRandomDocument() {
    return new Promise(function(resolve, reject) {
        dataRedis.client().srandmember("setGeneralHotDocuments", function(error, broadcastId) {
            if (error) {
                reject(error)
            }
            if (!broadcastId) {
                reject({
                    error: 'broadcastId null',
                })
            }
            resolve(broadcastId)
        })
    })
}

module.exports = {
    AddDocument,
    PickRandomDocument,
}
