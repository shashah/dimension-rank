const redis = require('redis')
const rfr = require('rfr')
const dataRedis = rfr('data/redis')

// Note: We are adding the *full* document.
async function AddBroadcast(broadcast) {
    console.log('AddBroadcast', {
        broadcast
    })
    const jsonString = JSON.stringify(broadcast)
    return new Promise(function(resolve, reject) {
        if (!broadcast) {
            reject({
                error: 'badArgument',
                detail: 'documentId',
            })
        } else {
            dataRedis.client().sadd("setGeneralHotDocuments", jsonString, function(error, result) {
                if (error) {
                    reject(error)
                } else {
                    resolve(result)
                }
            })
        }
    })
}

module.exports = {
    AddBroadcast,
}
