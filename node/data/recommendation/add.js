const rfr = require('rfr')

const objects = rfr('data/objects')
const config = rfr('data/config')
const dataRedis = rfr('data/redis')

function InternalAdd(params, output) {
    console.log('InternalAdd', {
        params
    })
    const queueName = config.PersonalQueueName(params.subjectId)
    dataRedis.client().rpush([queueName, JSON.stringify(params.broadcastResult)], function(error) {
        console.log('InternalAdd', {
            error
        })
        if (error) {
            output.reject(error)
        } else {
            output.resolve(undefined)
        }
    })
}

async function LookupBroadcast(params, output) {
    objects.Broadcast.findById(params.broadcastId).then(broadcastResult => {
        if (broadcastResult) {
            InternalAdd({
                ...params,
                broadcastResult,
            }, output)
        } else {
            output.reject({
                error: 'indexNotFound',
            })
        }
    }).catch(error => {
        output.reject(error)
    })
}

async function AddRecommendation(subjectId, broadcastId) {
    console.log('AddRecommendation', {subjectId}, {broadcastId})
    return new Promise(function(resolve, reject) {
        LookupBroadcast({
            subjectId,
            broadcastId
        }, {
            resolve,
            reject
        })
    })
}

module.exports = {
    AddRecommendation,
}
