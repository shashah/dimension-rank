const cheerio = require('cheerio')
const getUrls = require('get-urls')
const fetch = require('node-fetch')
const mongoose = require('mongoose')
const rfr = require('rfr')

const objects = rfr('data/objects')
const dataRedis = rfr('data/redis')
const config = rfr('data/config')

async function InternalPushRecommendations(subjectId, broadcastStrings) {
    return new Promise(function(resolve, reject) {
        if (broadcastStrings.length == 0) {
            resolve(undefined)
        } else {
            const queueName = config.PersonalQueueName(subjectId)
            console.log('InternalPushRecommendations', {
                queueName
            })
            var query = [queueName]
            query = query.concat(broadcastStrings)
            console.log('InternalPushRecommendations', 'query', query)
            dataRedis.client().rpush(query, function(error) {
                if (error) {
                    reject(error)
                } else {
                    resolve(undefined)
                }
            })
        }
    })
}

// Note: If there are no hot documents, then none will be added.
function ReplenishFromHotSet(params, output) {
    dataRedis.client().srandmember(['setGeneralHotDocuments', config.sizeOfHotBatch], function(error, genericBatch) {
        console.log('ReplenishFromHotSet', {
            error
        })
        console.log('ReplenishFromHotSet', {
            genericBatch
        })
        if (error) {
            output.reject(error)
        } else if (!genericBatch) {
            output.reject({
                error: 'genericBatch null',
            })
        } else {
            try {
                const product = InternalPushRecommendations(params.subjectId, genericBatch)
                output.resolve(product)
            } catch (error) {
                output.reject(error)
            }
        }
    })
}

function ReplenishFromPersonal(params, output) {
    console.log('ReplenishFromPersonal', {
        params
    })
    const queueName = config.PersonalQueueName(params.subjectId)
    dataRedis.client().llen(queueName, function(error, listSize) {
        console.log('ReplenishFromPersonal', {
            error,
        })
        console.log('ReplenishFromPersonal', {
            listSize,
        })
        if (error) {
            output.reject(error)
        } else {
            try {
                if (listSize < config.sizeForHotReplenish) {
                    ReplenishFromHotSet(params, output)
                } else {
                    output.resolve(undefined)
                }
            } catch (error) {
                output.reject(error)
            }
        }
    })
}

// Returns: Promise<Array<Broadcast>>
//
// Result: We add 'batchSize' recommendations for the personal recommendations
// queue of 'subjectId'.
//
// We will fill as much as possible with personal recommendations.
//
// After that, we will fill the rest with completely random recommendations
// from the generically popular set.
async function ReplenishRecommendations(subjectId, batchSize) {
    // Step 1: check how many recommendations are on 'hotQueue', compare this to
    // 'minHotQueueSize.

    // Step 2: get up to 'sizeRecommendationBatch' broadcasts from the "fresh
    // recommendations".  Step 3: get up to 'sizeRecommendationBatch' broadcasts
    // from the "old favorites".

    // Let 'sizeFreshBatch' be the size of the fresh batch.
    // Let 'sizeShortcoming' be 'sizeRecommendationBatch - sizeFreshBatch'.

    // Step 4a: Add the 'sizeFreshBatch' examples from the fresh batch.
    // Step 4b: Add 'sizeShortcoming' examples from the old favorites.
    return new Promise(function(resolve, reject) {
        ReplenishFromPersonal({
            subjectId,
            batchSize,
        }, {
            resolve,
            reject,
        })
    })
}

module.exports = {
    ReplenishRecommendations,
}
