const rfr = require('rfr')

const objects = rfr('data/objects')
const config = rfr('data/config')
const dataRedis = rfr('data/redis')

function TryPopGeneric(params, output) {
    console.log('TryPopGeneric', {
        params
    })
    dataRedis.client().srandmember(['setGeneralHotDocuments', 1], function(error, broadcastString) {
        console.log('TryPopGeneric', {
            error
        }, {
            broadcastString
        })
        if (error) {
            output.reject(error)
        } else {
            output.resolve(broadcastString)
        }
    })
}

function TryPopPersonal(params, output) {
    console.log('TryPopPersonal', {
        params
    })
    const queueName = config.PersonalQueueName(params.subjectId)
    console.log('TryPopPersonal', {
        queueName
    })
    dataRedis.client().lpop(queueName, function(error, broadcastString) {
        console.log('TryPopPersonal', {
            error
        }, {
            broadcastString
        })
        if (error) {
            output.reject(error)
        } else {
            if (broadcastString) {
                output.resolve(broadcastString)
            } else {
                TryPopGeneric(params, output)
            }
        }
    })
}

async function PopRecommendation(subjectId) {
    console.log('PopRecommendation', 'subjectId', subjectId)
    return new Promise(function(resolve, reject) {
        TryPopPersonal({
            subjectId
        }, {
            resolve,
            reject
        })
    })
}

module.exports = {
    PopRecommendation,
}
