const mongoose = require('mongoose')

async function ResetMongoClient(options) {
	mongoose.connect(options.uri, {
			useNewUrlParser: true,
			useUnifiedTopology: true
	})
}

module.exports = {
    ResetMongoClient,
}


