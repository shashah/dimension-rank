const cassandra = require('cassandra-driver')

var globalClient = null

// We use a global client because NodeJS is single-threaded.
function ResetCassandraClient(options) {
	console.log('ResetCassandraClient', {options})
	globalClient = new cassandra.Client(options)

	return new Promise(function(resolve, reject) {
		globalClient.connect(function (error) {
			if (error) {
				reject(error)
			} else {
				resolve(undefined)
			}
		})
	})
}

function MakeColumnString (columnNames) {
	return columnNames.join(', ')
}

function MakeQuestionString(columnNames) {
	var buffer = ''
	for (var i = 0; i < columnNames.length; ++i) {
		if (i > 0) {
			buffer += ', '
		}
		buffer += '?'
	}
	return buffer
}

function MakeInsertQuery(tableName, columnNames, values) {
	var buffer = 'INSERT INTO '
	buffer += tableName
	buffer += ' (id, '
	const columnString = MakeColumnString(columnNames)
	buffer += columnString
	buffer += ') VALUES (now(), '
	const questionString = MakeQuestionString(columnNames)
	buffer += questionString
	buffer += ')'
	return buffer
}

function MakeParamList(columnNames, values) {
	console.log('MakeParamList', {columnNames}, {values})
	var buffer = []
	for (var i = 0; i < columnNames.length; ++i) {
		const columnName = columnNames[i]
		console.log('MakeParamList', {columnName})
		const value = values[columnName]
		console.log('MakeParamList', {value})
		buffer.push(value)
	}
	return buffer
}

// Returns the promise from the Cassandra library.
function Insert(tableName, columnNames, values) {
	const query = MakeInsertQuery(tableName, columnNames, values)
	const valueList = MakeParamList(columnNames, values)
	console.log('Insert', {query})
	console.log('Insert', {valueList})
	return globalClient.execute(query, valueList)
}

module.exports = {
    ResetCassandraClient,
		MakeInsertQuery,
		MakeParamList,
		Insert,
}
