const rfr = require('rfr')
const cassandraData = rfr('data/cassandra')

async function LogLoginAttempt(params) {
    const tableName = 'login_attempt_event'
    const columnNames = ['email']
    return cassandraData.Insert(tableName, columnNames, params)
}

async function LogLoginFollowup(params) {
    const tableName = 'login_followup_event'
    const columnNames = ['user_id']
    return cassandraData.Insert(tableName, columnNames, params)
}

async function LogApplication(params) {
    const tableName = 'application_event'
    const columnNames = ['email', 'fullName', 'profile']
    return cassandraData.Insert(tableName, columnNames, params)
}

async function LogLogout(params) {
    const tableName = 'logout_event'
    const columnNames = ['user_id']
    return cassandraData.Insert(tableName, columnNames, params)
}

async function LogLabel(params) {
    const tableName = 'label_event'
    const columnNames = ['user_id', 'object_id', 'label_name']
    return cassandraData.Insert(tableName, columnNames, params)
}

async function LogBroadcast(params) {
    const tableName = 'broadcast_event'
    const columnNames = ['user_id', 'textarea']
    return cassandraData.Insert(tableName, columnNames, params)
}

module.exports = {
    LogLoginAttempt,
    LogLoginFollowup,
    LogApplication,
    LogLogout,
    LogLabel,
    LogBroadcast,
}
