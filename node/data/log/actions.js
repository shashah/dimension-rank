const rfr = require('rfr')
const cassandraData = rfr('data/cassandra')

async function LogRecommendationAction(params) {
    const tableName = 'recommendation_action'
    const columnNames = ['user_id', 'object_id', 'label_name', 'prediction', 'details']
    return cassandraData.Insert(tableName, columnNames, params)
}

async function LogLabelTrainAction(params) {
    const tableName = 'label_train_action'
    const columnNames = ['user_id', 'object_id', 'label_name', 'gold', 'prediction', 'details']
    return cassandraData.Insert(tableName, columnNames, params)
}

module.exports = {
    LogRecommendationAction,
    LogLabelTrainAction,
}
