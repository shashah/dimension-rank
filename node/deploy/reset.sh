CONFIG=$1
echo CONFIG [${CONFIG}]

if [ -z ${CONFIG} ]; then echo "provide CONFIG"; exit 1; fi

VERSIONED_FILE="${PWD}/deploy/node.${CONFIG}.conf"
SUPERVISOR_FILE="/etc/supervisor/conf.d/node.conf"

echo VERSIONED_FILE [${VERSIONED_FILE}]
echo SUPERVISOR_FILE [${SUPERVISOR_FILE}]

sudo cp -f ${VERSIONED_FILE} ${SUPERVISOR_FILE}

