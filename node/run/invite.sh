ID=$1

if [ -z ${ID} ]; then
	echo not set;
fi
node tools/inviteUser.js --options ~/secrets/node.production.json --applicationId ${ID}
