const cassandra = require('cassandra-driver')

const client = new cassandra.Client({
  contactPoints: ['localhost'],
  localDataCenter: 'datacenter1',
  keyspace: 'deeprev'
})


const query = 'SELECT * FROM boolean_decision'

client.execute(query, [ ])
  .then(result => console.log({result}))

