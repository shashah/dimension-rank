const fs = require('fs')
const mongoose = require('mongoose')
const cheerio = require('cheerio')
const getUrls = require('get-urls')
const fetch = require('node-fetch')
const rfr = require('rfr')

const objects = rfr('data/objects')
const createBroadcast = rfr('data/broadcast/create')
const labelBroadcast = rfr('data/label/create')
const config = rfr('data/config')

// Constants.
const numTrainingRounds = 1000

// Command-line arguments.
const secretsFname = process.argv[2]
const subjectId = process.argv[3]
const winnerId = process.argv[4]
const loserId = process.argv[5]
if (!(subjectId && winnerId && loserId)) {
    console.log('not enough arguments', subjectId, winnerId, loserId)
    process.exit(1)
}

// Read in the secrets file.
if (!secretsFname) {
	console.log('main', "No file name given")
	process.exit(1)
}
try {
	const fileText = fs.readFileSync(secretsFname)
	const json = JSON.parse(fileText)
	console.log('main', {json})
	mongoose.connect(json.mongoUri, {useNewUrlParser: true,  useUnifiedTopology: true})
} catch (error) {
	console.log('main', "Error during initialization.", {error})
	process.exit(1)
}

// Asyncronous 'main' method.
async function Main() {
    try {
        const subject = await objects.User.findById(subjectId)
        // console.log('Main', 'subject', subject)

        const winner = await objects.User.findById(winnerId)
        // console.log('Main', 'winner', winner)

        const loser = await objects.User.findById(loserId)
        // console.log('Main', 'loser', loser)

				if (!(subject && winner && loser)) {
					console.log('main', 'argument missing', subject, winner, loser)
					process.exit(1)
				}

        for (var i = 0; i < numTrainingRounds; ++i) {
						console.log('Main', 'i', i)
            // Step 1: Create the winnerBroadcast and like it.
            const winnerBroadcast = await createBroadcast.CreateBroadcast('textarea', winner)
            // console.log('Main', 'winnerBroadcast', winnerBroadcast)
            const winnerLabel = await labelBroadcast.LabelBroadcast(subjectId, winnerBroadcast._id, config.keyHotnessTarget, true)
            // console.log('Main', 'winnerLabel', winnerLabel)

            // Step 2: Create the loserBroadcast and like it.
            const loserBroadcast = await createBroadcast.CreateBroadcast('textarea', loser)
            // console.log('Main', 'loserBroadcast', loserBroadcast)
            const loserLabel = await labelBroadcast.LabelBroadcast(subjectId, loserBroadcast._id, config.keyHotnessTarget, false)
            // console.log('Main', 'loserLabel', loserLabel)
        }
        process.exit(0)
    } catch (error) {
        console.log('Main', 'error', error)
        process.exit(1)

    }
}
Main()
