const sgMail = require('@sendgrid/mail');

const rfr = require('rfr')

const globalOptions = rfr('globalOptions')
const objects = rfr('data/objects')
const mongoData = rfr('data/mongo')

function MakeText(params) {
    return `
		Hello ${params.fullName},
		
		We have received your application. We'll get back to you as soon as possible.
		
		Think different, again.
	`
}

function MakeHtml(params) {
    return `
		Hello ${params.fullName},<br>
		<br>
		We have received your application. We'll get back to you as soon as possible.
		<br>
		Think different, again.
	`
}

async function SendTheEmail(email, fullName) {
    console.log('SendTheEmail', {
        email,
        fullName
    })
    sgMail.setApiKey(globalOptions.options().sendgridApiKey)
    const params = {
        email,
        fullName,
    }
    const msg = {
        to: params.email,
        from: 'Deep Revelations <mailman@deeprevelations.com>',
        subject: 'Invitation to Join Enclosed!',
        text: MakeText(params),
        html: MakeHtml(params),
    }
    return sgMail.send(msg)
}

module.exports = {
    SendTheEmail,
}
