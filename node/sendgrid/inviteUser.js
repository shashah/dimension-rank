const sgMail = require('@sendgrid/mail');

const rfr = require('rfr')

const globalOptions = rfr('globalOptions')
const objects = rfr('data/objects')
const createEmailAuthorization = rfr('data/emailAuthorization/create')
const mongoData = rfr('data/mongo')

function MakeText(params) {
    return `
		Hello ${params.fullName},
		
		You have been invited to join Deep Revelations!
		
		Use this link to sign up:
		${params.url}
		
		Think different, again.
	`
}

function MakeHtml(params) {
    return `
		Hello ${params.fullName},<br>
		<br>
		You have been invited to join Deep Revelations!<br>
		<br>
		Use this link to sign up:<br>
		<a href="${params.url}">${params.url}</a><br>
		<br>
		Think different, again.
	`
}

async function SendTheEmail(params) {
    console.log('SendTheEmail', {
        globalOptions
    })
    sgMail.setApiKey(globalOptions.options().sendgridApiKey)
    const msg = {
        to: params.email,
        from: 'Deep Revelations <mailman@deeprevelations.com>',
        subject: 'Invitation to Join Enclosed!',
        text: MakeText(params),
        html: MakeHtml(params),
    }
    return sgMail.send(msg)
}

module.exports = {
    SendTheEmail,
}
