const rfr = require('rfr')

const objects = rfr('data/objects')
const createEmailAuthorization = rfr('data/emailAuthorization/create')

async function CreateEmailAuthorization(email) {
    console.log('CreateEmailAuthorization', email)
    createEmailAuthorization.CreateEmailAuthorization(email).then(function(result) {
        console.log({
            result
        })
        process.exit(0)
    }).catch(function(error) {
        console.log({
            error
        })
        process.exit(1)
    })
}

const email = process.argv[2]
if (email == null) {
    console.log('email is null')
    process.exit(1)
}

console.log('process.argv', process.argv)

const mongoose = require('mongoose')

const uri = process.env.DEEPREV_MONGOOSE_URI
console.log('mongoose uri', uri)
mongoose.connect(uri, {
    useNewUrlParser: true,
    useUnifiedTopology: true
})

CreateEmailAuthorization(email)