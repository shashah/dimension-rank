const nopt = require('nopt')
const rfr = require('rfr')

const globalOptions = rfr('globalOptions')
const dataCassandra = rfr('data/cassandra')
const logEvents = rfr('data/log/events')

const parsedArgv = nopt({
        'email': String,
    }, {},
    process.argv, 2)

console.log('email', parsedArgv.email)

async function Main(params) {
	const cassandraResult = await dataCassandra.ResetCassandraClient({
		host: 'localhost',
		dataCenter: 'datacenter1',
		keyspace: 'development',
	})
	console.log('Main', {cassandraResult})

	const logOptions = {email: parsedArgv.email}
	console.log('Main', {logOptions})
	const result = await logEvents.LogLoginAttempt(logOptions)

	console.log('Main', {result})
}

Main()
