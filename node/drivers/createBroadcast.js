const nopt = require('nopt')
const rfr = require('rfr')

const objects = rfr('data/objects')
const simpleHotSet = rfr('data/genericHot/add')
const createBroadcast = rfr('data/broadcast/create')
const mongoData = rfr('data/mongo')

const parsedArgv = nopt({
    'textarea': String,
    'userName': String,
    'referenceId': String,
    'polarity': String,
}, {}, process.argv, 2)
console.log(parsedArgv)

mongoData.ResetMongoClient(process.env.DEEPREV_MONGOOSE_URI)

const ok = parsedArgv.textarea && parsedArgv.userName
if (!ok) {
    console.log('not ok')
    process.exit(1)
}


objects.User.findOne({
    userName: parsedArgv.userName,
}).then(userResult => {
    console.log({
        userResult
    })
    createBroadcast.CreateBroadcast(parsedArgv.textarea, userResult, parsedArgv.referenceId, parsedArgv.polarity).then(broadcastResult => {
        console.log('Main', {
            broadcastResult
        })
    }).catch(error => {
        console.log('Main', {
            error
        })
        process.exit(1)
    })
}).catch(error => {
    console.log('Main', {
        error
    })
    process.exit(1)
})