const mongoose = require('mongoose')
const cheerio = require('cheerio')
const getUrls = require('get-urls')
const fetch = require('node-fetch')
const rfr = require('rfr')

const objects = require('data/objects')
const createUser = require('data/user/create')

const uri = process.env.DEEPREV_MONGOOSE_URI
console.log('mongoose uri', uri)
mongoose.connect(uri, {
    useNewUrlParser: true,
    useUnifiedTopology: true
})


const userName = process.argv[2]
const email = process.argv[3]

if (!(userName && email)) {
	console.log('not enough arguments', userName, email)
	process.exit(1)
}
createUser.TryCreateUser(userName, email).then(result => {
	console.log('final', 'result', result)
	process.exit(0)
}).catch(error => {
	console.log('final', 'error', error)
	process.exit(1)
})
