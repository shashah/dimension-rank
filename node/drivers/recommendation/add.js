const nopt = require('nopt')
const rfr = require('rfr')

const mongoData = rfr('data/mongo')
const addRecommendation = rfr('data/recommendation/add')

const parsedArgv = nopt({
        'userId': String,
        'broadcastId': String,
    }, {},
    process.argv, 2)

console.log(parsedArgv)

mongoData.ResetMongoClient(process.env.DEEPREV_MONGOOSE_URI)

async function Main() {
	try {
		const result = await addRecommendation.AddRecommendation(parsedArgv.userId, parsedArgv.broadcastId)
    console.log('Main', {result})
		process.exit(0)
	} catch (error) {
    console.log('Main', {error})
    process.exit(1)
	}
}

Main()
