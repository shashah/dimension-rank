const nopt = require('nopt')
const rfr = require('rfr')

const mongoData = rfr('data/mongo')
const popRecommendation = rfr('data/recommendation/pop')

const parsedArgv = nopt({
        'userId': String,
    }, {},
    process.argv, 2)

console.log(parsedArgv)

mongoData.ResetMongoClient(process.env.DEEPREV_MONGOOSE_URI)

popRecommendation.PopRecommendation(parsedArgv.userId).then(result => {
    console.log('Main', 'result', result)
    process.exit(0)
}).catch(error => {
    console.log('Main', 'error', error)
    process.exit(1)
})
