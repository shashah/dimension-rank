const nopt = require('nopt')
const rfr = require('rfr')

const mongoData = rfr('data/mongo')
const objects = rfr('data/objects')
const popRecommendation = rfr('data/recommendation/pop')
const replenishRecommendations = rfr('data/recommendation/replenish')

const args = nopt({
        "userName": String,
    }, {
    },
    process.argv, 2)

console.log(args)

mongoData.ResetMongoClient(process.env.DEEPREV_MONGOOSE_URI)

async function Main() {
	try {
		const userName = args.userName
		console.log('Main', {userName})
		const user = await objects.User.findOne({userName})
		console.log('Main', {user})
		if (!user) {
			process.exit(1)
		}

		replenishRecommendations.ReplenishRecommendations(user._id).then(result => {
			console.log('Main', 'result', result)
			process.exit(0)
		}).catch(error => {
			console.log('Main', 'error', error)
			process.exit(1)
		})
	} catch (error) {
			console.log('Main', 'error', error)
			process.exit(1)
	}
}

Main()
