const mongoose = require('mongoose')
const nopt = require('nopt')
const rfr = require('rfr')

const createLabel = rfr('data/label/create')
const mongoData = rfr('data/mongo')

const args = nopt({
    'userId': String,
    'broadcastId': String,
    'name': String,
    'value': String,
}, {}, process.argv, 2)
console.log(args)

mongoData.ResetMongoClient(process.env.DEEPREV_MONGOOSE_URI)

console.log('Main', 'args', args)
if (!(args.userId && args.broadcastId && args.name && args.value)) {
	process.exit(1)
}

console.log('createLabel', createLabel)
createLabel.LabelBroadcast(args.userId, args.broadcastId, args.name, args.value).then(result => {
	console.log('Main', 'result', result)
	process.exit(0)
}).catch(error => {
	console.log('Main', 'error', error)
	process.exit(1)
})
