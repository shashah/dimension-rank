# drivers

`drivers` is a directory for *thin wrappers* around API calls.

`tools` is for programs that either:

	* wrap more than one API function call
	* do not wrap any function calls at all (e.g., test redis config)
