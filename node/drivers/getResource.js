const nopt = require('nopt')
const mongoose = require('mongoose')
const rfr = require('rfr')

const resources = rfr('data/resource/get')
const mongoData = rfr('data/mongo')

const parsedArgv = nopt({
    'url': String,
}, {}, process.argv, 2)
console.log(parsedArgv)

mongoData.ResetMongoClient(process.env.DEEPREV_MONGOOSE_URI)

resources.BlockingResouceFromUrl(parsedArgv.url).then(result => {
    console.log('Final result:', result)
    process.exit(0)
}).catch(error => {
    console.log('error', error)
    process.exit(1)
})
