const mongoose = require('mongoose')
const cheerio = require('cheerio')
const getUrls = require('get-urls')
const fetch = require('node-fetch')
const nopt = require('nopt')

const rfr = require('rfr')
const resetCookieToken = rfr('data/cookieToken/reset')
const mongoData = rfr('data/mongo')
const objects = rfr('data/objects')

// Initialize.
const parsedArgv = nopt({
        'userName': String,
    }, {},
    process.argv, 2)
console.log(parsedArgv)
mongoData.ResetMongoClient(process.env.DEEPREV_MONGOOSE_URI)

async function CreateCookieToken(params, output) {
    resetCookieToken.ResetCookieToken(params.user).then(cookieToken => {
        if (cookieToken) {
            output.resolve(cookieToken)
        } else {
            output.reject({
                error: 'objectNull',
                detail: 'cookieToken'
            })
        }
    }).catch(error => {
        response.status(200).json({
            success: false,
            error,
        })
    })
}

async function LookupUserByUserName(params, output) {
    objects.User.findOne({
        userName: params.userName
    }).then(function(userResult) {
        if (userResult) {
            CreateCookieToken({
                user: userResult
            }, output)
        } else {
            output.reject({
                error: 'objectNotFound',
                detail: 'User,email'
            })
        }
    }).catch(function(error) {
        output.reject(error)
    })
}

async function CookieTokenForUserName(userName) {
    return new Promise(function(resolve, reject) {
        LookupUserByUserName({
            userName
        }, {
            resolve,
            reject
        })
    })
}

CookieTokenForUserName(parsedArgv.userName).then(result => {
    console.log('Main', {
        result
    })
    process.exit(0)
}).catch(error => {
    console.log('Main', {
        error
    })
    process.exit(0)
})
