const nopt = require('nopt')
const rfr = require('rfr')

const addGenericHot = rfr('data/genericHot/add')
const mongoData = rfr('data/mongo')
const popRecommendation = rfr('data/recommendation/pop')
const objects = rfr('data/objects')

const parsedArgv = nopt({
        'broadcastId': String,
    }, {},
    process.argv, 2)

console.log(parsedArgv)

mongoData.ResetMongoClient(process.env.DEEPREV_MONGOOSE_URI)

async function Main() {
	try {
		const broadcastId = parsedArgv.broadcastId
		console.log('Main', {broadcastId})
		const broadcastResult = await objects.Broadcast.findById(broadcastId)
		console.log('Main', {broadcastResult})
		if (broadcastResult) {
			const addResult = await addGenericHot.AddBroadcast(broadcastResult)
			console.log('Main', {addResult})
		}
		process.exit(0)
	} catch(error) {
		console.log('Main', {error})
		process.exit(1)
	}
}

Main()
