const mongoose = require('mongoose')
const express = require('express')
const router = express.Router()
const rfr = require('rfr')

const objects = rfr('data/objects')
const applicationReceived = rfr('sendgrid/applicationReceived')
const logEvents = rfr('data/log/events')

async function LogApplicationEvent(params, response) {
    logEvents.LogApplication(params).then(response => {
        // pass quietly
    }).catch(error => {
        console.log('LogApplicationEvent', {
            error
        })
    })
}

async function SendReceivedEmail(params, response) {
    applicationReceived.SendTheEmail(params.email, params.fullName).then(result => {
        LogApplicationEvent(params, response)
        response.status(200).json({
            success: true,
        })
    }).catch(error => {
        response.status(200).json({
            success: false,
        })
    })
}

async function CreateApplication(params, response) {
    console.log('CreateApplication', {
        params
    })
    const application = new objects.Application({
        _id: new mongoose.Types.ObjectId(),
        email: params.email,
        fullName: params.fullName,
        profile: params.profile,
        creationTime: Date.now(),
    })
    console.log('CreateApplication', {
        application
    })

    application.save().then(result => {
        console.log('CreateApplication', {
            result
        })
        SendReceivedEmail(params, response)
    }).catch(error => {
        console.log('CreateApplication', {
            error
        })
        response.status(200).json({
            success: false,
            error,
        })
    })
}

async function ParseArguments(args, response) {
    console.log('ParseArguments', {
        args
    })

    const fullName = args.fullName
    const email = args.email
    const profile = args.profile

    if (fullName == undefined) {
        console.log('ParseArguments', {
            fullName
        })
        response.status(200).json({
            success: false,
            error: 'missingArgument',
            detail: 'fullName',
        })
    } else if (email == undefined) {
        console.log('ParseArguments', {
            email
        })
        response.status(200).json({
            success: false,
            error: 'missingArgument',
            detail: 'email',
        })
    } else if (profile == undefined) {
        console.log('ParseArguments', {
            profile
        })
        response.status(200).json({
            success: false,
            error: 'missingArgument',
            detail: 'profile',
        })
    } else {
        console.log('calling')
        CreateApplication({
            fullName,
            email,
            profile
        }, response)
    }
}

router.post('', (request, response, next) => {
    ParseArguments(request.body, response)
})

module.exports = router
