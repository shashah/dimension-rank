const express = require('express')
const router = express.Router()
const rfr = require('rfr')

const popRecommendation = rfr('data/recommendation/pop')
const login = rfr('middleware/login')

async function PickARandomDocument(authenticatedUser, response) {
	console.log('PickARandomDocument', {authenticatedUser})
	popRecommendation.PopRecommendation(authenticatedUser._id).then(broadcastString => {
		const broadcast = JSON.parse(broadcastString)
		response.status(200).json({
				success: true,
			broadcast,
		})
	}).catch(error => {
		response.status(200).json({
				success: false,
			error,
		})
	})
}


router.post('', login.Required, (request, response, next) => {
    PickARandomDocument(request.authenticatedUser, response)
})

module.exports = router
