const express = require('express')
const router = express.Router()
const rfr = require('rfr')

const objects = rfr('data/objects')
const findCookieToken = rfr('data/cookieToken/find')

async function DoValidateCookieToken(request, response) {
    console.log('DoValidateCookieToken', 'request.body', request.body)
    if (request.body && request.body.cookieToken) {
        const cookieToken = request.body.cookieToken
        console.log('DoValidateCookieToken', 'cookieToken', cookieToken)
        findCookieToken.FindCookieTokenUser(cookieToken).then(authenticatedUser => {
            response.status(200).json({
                success: true,
                authenticatedUser,
            })
				}).catch(error => {
            response.status(200).json({
                success: false,
                error,
            })
        })
    } else {
        console.log('DoValidateCookieToken', 'request.userId not found')
        response.status(200).json({
            success: false,
            error: 'MissingArgument',
						body: request.body,
        })
    }
}

router.post('', (request, response, next) => {
    DoValidateCookieToken(request, response)
})

module.exports = router
