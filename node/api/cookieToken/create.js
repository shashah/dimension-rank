const mongoose = require('mongoose')
const express = require('express')
const router = express.Router()
const rfr = require('rfr')

const objects = rfr('data/objects')
const resetCookieToken = rfr('data/cookieToken/reset')

async function CreateCookieToken(params, response) {
console.log('CreateCookieToken', {params})
    resetCookieToken.ResetCookieToken(params.user).then(cookieToken => {
        response.status(200).json({
            success: true,
            cookieToken,
						userName: params.user.userName,
        })
    }).catch(error => {
        response.status(200).json({
            success: false,
            error,
        })
    })
}

async function LookupUserByEmail(params, response) {
    objects.User.findOne({
        email: params.email
    }).then(function(userResult) {
        if (userResult) {
            CreateCookieToken({user: userResult}, response)
        } else {
            response.status(200).json({
                success: false,
                error: 'emailTokenNotFound',
            })
        }
    }).catch(function(error) {
        response.status(200).json({
            success: false,
            error,
        })
    })
}
async function LookupEmailByToken(params, response) {
	console.log('LookupEmailByToken', {params})
    objects.EmailToken.findById(params.emailToken).then(function(tokenResult) {
        if (tokenResult) {
            LookupUserByEmail({
                email: tokenResult.email,
            }, response)
        } else {
            response.status(200).json({
                success: false,
                error: 'emailTokenNotFound',
            })
        }
    }).catch(function(error) {
        response.status(200).json({
            success: false,
            error,
        })
    })
}

// 'request' must contain a positional 'emailToken'
async function ParseArguments(request, response) {
    console.log('ParseArguments', request.body)
    const emailToken = request.body.emailToken
    if (!emailToken) {
        response.status(200).json({
            success: false,
            error: 'argumentMissing',
            detail: 'emailToken',
        })
    } else {
        LookupEmailByToken({
            emailToken
        }, response)
    }
}

router.post('', (request, response, next) => {
    console.log('authtoken create')
    ParseArguments(request, response)
})

module.exports = router
