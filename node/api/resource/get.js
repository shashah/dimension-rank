// 'resource/get' is 
const express = require('express')
const router = express.Router()

const rfr = require('rfr')

const objects = rfr('data/objects')
const getResource = rfr('data/resource/get')

async function ReadResource(url, response) {
	getResource.BlockingResouceFromUrl(url).then(resource => {
		response.status(200).json({
			success: true,
			resource,
		})
	}).catch(error => {
		response.status(200).json({
			success: false,
			error,
		})
	})
}

router.post('', (request, response, next) => {
	const url = request.body.url
	if (!url) {
		response.status(200).json({
			success: false,
			error: 'missingArgument',
			errorDetail: 'url',
		})
	} else {
		ReadResource(url, response)
	}
})

module.exports = router


