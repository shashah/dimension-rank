const mongoose = require('mongoose')
const express = require('express')
const router = express.Router()
const multer = require('multer')

const rfr = require('rfr')
const objects = require('data/objects')
const config = require('data/config')

const uploadPath = process.env.DEEPREV_MULTER_UPLOAD_PATH

const storage = multer.diskStorage({
  destination(request, file, cb) {
    cb(null, uploadPath)
  },
  filename(request, file, cb) {
    // 1. Start with a uuid.
    var result = uuid.v1()
    // 2. If there is an extension, add it.
    const parts = file.originalname.split('.')
    if (parts.size > 1) {
      result += '.'
      result += parts[parts.length - 1]
    }
    cb(null, result)
  }
})

const fileFilter = (request, file, cb) => {
  if (true) {
    // if (file.mimetype == 'image/jpeg' || file.mimetype == 'image/png') {
    cb(null, true)
  } else {
    cb(null, false)
  }
}

const upload = multer({
  storage: storage,
  limits: {
    fileSize: 1024 * 1024 * 5
  },
  fileFilter: fileFilter
})

async function DoUploadImage(request, response) {
    const userId = emailToken.CheckToken(emailToken)
    if (userId != null) {
        response.status(200).json({
            success: true
        })
    } else {
        response.status(200).json({
            success: false
        })
    }
}

router.post('', upload.single('uploadImage'), (request, response, next) => {
    DoUploadImage(request, response)
})

module.exports = router

