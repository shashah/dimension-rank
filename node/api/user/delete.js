const express = require('express')
const router = express.Router()
const rfr = require('rfr')

const objects = rfr('data/objects')

async function DeleteAndRespond(params, response) {
    const query = {
        _id: params.userId,
    }
    console.log('DeleteAndRespond', {
        query
    })
    objects.User.deleteOne(query).then(broadcast => {
        response.status(200).json({
            success: true,
            broadcast,
        })
    }).catch(error => {
        response.status(200).json({
            success: false,
            error,
        })
    })
}

async function ParseArguments(request, response) {
    console.log('ParseArguments', 'request.body', request.body)
    const userId = request.body.userId
    if (!userId) {
        response.status(200).json({
            success: false,
            error: 'argumentMising',
            detail: 'userId',
        })
    } else {
        DeleteAndRespond({
            userId
        }, response)
    }
}

router.post('', (request, response, next) => {
    ParseArguments(request, response)
})

module.exports = router
