const express = require('express')
const router = express.Router()
const rfr = require('rfr')

const objects = rfr('data/objects')
const login = rfr('middleware/login')

// 'fullUser' a tuple
// 'isSelf' a boolean
function MaskUser(fullUser, isSelf) {
    var buffer = {
        _id: fullUser._id,
        userName: fullUser.userName,
        profile: fullUser.profile,
        description: fullUser.description,
    }
    if (isSelf) {
        buffer.email = fullUser.email
    }
    return buffer
}

async function ReadUser(params, response) {
    console.log('ReadUser', params.userName)
    objects.User.findOne({
        userName: params.userName,
    }).then(user => {
        const userView = MaskUser(user, params.isSelf)
        response.status(200).json({
            success: true,
            userView,
        })
    }).catch(error => {
        response.status(200).json({
            success: false,
            error,
        })
    })
}

async function ParseArguments(request, response) {
    // 'bodyName' refers to an argument from the body.
    const bodyName = request.body.userName
    const cookieName = request.authenticatedUser ?
        request.authenticatedUser.userName : null
    if (bodyName) {
        ReadUser({
            userName: bodyName,
            isSelf: false,
        }, response)
    } else if (cookieName) {
        ReadUser({
            userName: cookieName,
            isSelf: true,
        }, response)
    } else {
        // We should never reach here.
        response.status(200).json({
            success: false,
            error: 'InternalError',
        })
    }
}

router.post('', login.Required, (request, response, next) => {
    ParseArguments(request, response)
})

module.exports = router
