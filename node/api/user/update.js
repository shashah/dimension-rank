const mongoose = require('mongoose')
const express = require('express')
const router = express.Router()

const rfr = require('rfr')
const objects = rfr('data/objects')
const login = rfr('middleware/login')

async function UpdateUser(params, response) {
    console.log('UpdateUser', {
        params
    })
    const idQuery = {
        _id: params.authenticatedUser._id
    }
    const setQuery = {
        $set: {
            profile: params.profile,
        }
    }
    console.log('UpdateUser', {
        idQuery
    }, {
        setQuery
    })
    objects.User.update(idQuery, setQuery).then(updateResult => {
        response.status(200).json({
            success: true,
        })
    }).catch(error => {
        response.status(200).json({
            success: false,
            error,
        })
    })
}

async function ParseArguments(request, response) {
    const profile = request.body.profile
    if (!profile) {
        response.status(200).json({
            success: false,
            error: 'missingArgument',
            requestBody: request.body,
        })
    } else {
        UpdateUser({
            authenticatedUser: request.authenticatedUser,
            profile
        }, response)
    }
}

router.post('', login.Required, (request, response, next) => {
	console.log('/user/update', {body: request.body})
    ParseArguments(request, response)
})

module.exports = router
