const express = require('express')
const router = express.Router()
const rfr = require('rfr')

const objects = rfr('data/objects')

async function ReadAndRespond(params, response) {
    objects.Broadcast.findById(params.broadcastId).then(broadcast => {
        response.status(200).json({
            success: true,
            broadcast,
        })
    }).catch(error => {
        response.status(200).json({
            success: false,
            error,
        })
    })
}

async function ParseArguments(request, response) {
    console.log('ParseArguments', 'request.body', request.body)
    const broadcastId = request.body.broadcastId
    if (!broadcastId) {
        response.status(200).json({
            success: false,
            error: 'argumentMising',
            detail: 'broadcastId',
        })
    } else {
        ReadAndRespond({broadcastId}, response)
		}
}

router.post('', (request, response, next) => {
	ParseArguments(request, response)
})

module.exports = router
