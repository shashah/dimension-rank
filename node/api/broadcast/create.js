const mongoose = require('mongoose')
const express = require('express')
const router = express.Router()
const rfr = require('rfr')

const objects = rfr('data/objects')
const login = rfr('middleware/login')
const createBroadcast = rfr('data/broadcast/create')
const addGenericHot = rfr('data/genericHot/add')

// Note: We are putting *all* broadcasts into the hot set right now. We must
// SOON change this to only be things that are actually 'good'.
async function AddBroadcastToHotSet(params, response) {
        console.log('AddBroadcastToHotSet', {params})
    addGenericHot.AddBroadcast(params.broadcastResult).then(addResult => {
        console.log('AddBroadcastToHotSet', {addResult})
        const result = {
            success: true,
            broadcast: params.broadcastResult,
        }
        console.log('AddBroadcastToHotSet', {
            result
        })
        response.status(200).json(result)
    }).catch(error => {
        response.status(200).json({
            success: false,
            error,
        })
    })
}

async function CreateTheBroadcast(params, response) {
    console.log('CreateTheBroadcast', {
        params
    })
    var channelDescriptor = params.channelDescriptor
    createBroadcast.CreateBroadcast(params.textarea, channelDescriptor, params.producerTuple, params.referenceId, params.polarity).then(broadcastResult => {
        console.log({broadcastResult})
        AddBroadcastToHotSet({
            ...params,
            broadcastResult
        }, response)
    }).catch(error => {
        response.status(200).json({
            success: false,
            error,
        })
    })

}

async function ParseArguments(request, response) {
    console.log('ParseArguments', 'request.body', request.body)
    const textarea = request.body.textarea
    const channelName = request.body.channelName
    const channelType = request.body.channelType
    const polarity = request.body.polarity
    const referenceId = request.body.referenceId

    const producerTuple = request.authenticatedUser

    console.log('ParseArguments', 'textarea', textarea)
    console.log('ParseArguments', 'producerTuple', producerTuple)
    if (!textarea) {
        response.status(200).json({
            success: false,
            error: 'textAreaIsNull',
        })
        return
    } else if (!channelType) {
        response.status(200).json({
            success: false,
            error: 'missingArgument',
            details: ['channelType'],
        })
        return
    } else if (!producerTuple) {
        response.status(200).json({
            success: false,
            error: 'notLoggedIn',
        })
        return
    }

		console.log('survived')

    const channelDescriptor = {
        channelName,
        channelType,
    }
    if (polarity && referenceId) {
        CreateTheBroadcast({
            textarea,
            channelDescriptor,
            producerTuple,
            polarity,
            referenceId,
        }, response)
    } else {
        CreateTheBroadcast({
            textarea,
            channelDescriptor,
            producerTuple,
            polarity: null,
            referenceId: null,
        }, response)
    }
}

router.post('', login.Required, (request, response, next) => {
    ParseArguments(request, response)
})

module.exports = router
