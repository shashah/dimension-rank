const express = require('express')
const app = express()
const BodyParser = require('body-parser')
const cors = require('cors')


app.use(BodyParser.urlencoded({ extended: false }))
app.use(BodyParser.json())
app.use(cors())

app.use('/api/application/create', require('./application/create'))

app.use('/api/broadcast/create', require('./broadcast/create'))
app.use('/api/broadcast/read', require('./broadcast/read'))
app.use('/api/broadcast/delete', require('./broadcast/delete'))

app.use('/api/cookieToken/create', require('./cookieToken/create'))
app.use('/api/cookieToken/validate', require('./cookieToken/validate'))
app.use('/api/cookieToken/delete', require('./cookieToken/delete'))

app.use('/api/emailToken/validate', require('./emailToken/validate'))
app.use('/api/emailToken/create', require('./emailToken/create'))

app.use('/api/resource/get', require('./resource/get'))

app.use('/api/user/create', require('./user/create'))
app.use('/api/user/update', require('./user/update'))
app.use('/api/user/read', require('./user/read'))
app.use('/api/user/delete', require('./user/delete'))

app.use('/api/feed/single', require('./feed/single'))

app.use('/api/label/create', require('./label/create'))


// Error handling
app.use((request, response, next) => {
  const error = new Error('Not found')
  error.status = 404
  next(error)
})

app.use((error, request, response, next) => {
  response.status(error.status || 500)
  response.json({
    error: {
      message: error.message
    }
  })
})

module.exports = app
