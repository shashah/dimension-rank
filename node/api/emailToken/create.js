const express = require('express')
const router = express.Router()
const mongoose = require('mongoose')
const rfr = require('rfr')

const globalOptions = rfr('globalOptions')
const objects = rfr('data/objects')
const findEmailAuthorization = rfr('data/emailAuthorization/find')
const sendEmailToken = rfr('sendgrid/emailToken')
const logEvents = rfr('data/log/events')

async function LogLoginAttempt(params, response) {
    logEvents.LogLoginAttempt(params).then(response => {
        // pass
    }).catch(error => {
        console.log('LogLoginAttempt', {
            error
        })
    })
}
async function SendEmailToken(params, response) {
    sendEmailToken.SendTheEmail(params).then(result => {
        // Tell the user to check their email.
        response.status(200).json({
            success: true,
        })
    }).catch(error => {
        response.status(200).json({
            success: false,
            error,
        })
    })
}

async function FinishCreateEmailToken(params, response) {
    console.log('DoCreateEmailToken', {
        params
    })
    const emailToken = new objects.EmailToken({
        _id: new mongoose.Types.ObjectId(),
        email: params.email,
    })

    emailToken.save().then(function(result) {
        const token = result._id
        console.log('DoCreateEmailToken', {
            result
        })
        console.log('DoCreateEmailToken', {
            token
        })
        const url = globalOptions.options().vueBasePath + '/login/cookieToken/' + token
        console.log('DoCreateEmailToken', {
            url
        })
        SendEmailToken({
            ...params,
            url
        }, response)

    }).catch(function(error) {
        console.log('DoCreateEmailToken', 'error', error)

        response.status(200).json({
            success: false,
            error,
        })

    })
}

async function CheckEmailIsAuthorized(params, response) {
    console.log('DoCreateEmailToken', {
        params
    })
    findEmailAuthorization.CheckEmailAuthorization(params.email).then(result => {
        if (result) {
            console.log('CheckEmailIsAuthorized', 'result', result)
            FinishCreateEmailToken(params, response)
        } else {
            response.status(200).json({
                success: false,
                emailNotAuthorized: true,
            })
        }
    }).catch(error => {
        response.status(200).json({
            success: false,
            error,
        })
    })
}

async function CheckEmailArgumentPresent(request, response) {
    console.log('CheckEmailArgumentPresent', 'body', request.body)
    if (request.body && request.body.email) {
        const email = request.body.email
        LogLoginAttempt({
            email
        })
        console.log('CheckEmailArgumentPresent', 'email', email)
        CheckEmailIsAuthorized({
            email
        }, response)
    } else {
        response.status(200).json({
            success: false,
            reason: 'No email in request.',
        })
    }
}

router.post('', (request, response, next) => {
    console.log('CheckEmailArgumentPresent', 'body', request.body)
    CheckEmailArgumentPresent(request, response)
})

module.exports = router
