const express = require('express')
const router = express.Router()
const rfr = require('rfr')
const objects = rfr('data/objects')

async function ValidateEmailToken(params, response) {
    console.log('ValidateEmailToken', {
        params
    })
    objects.EmailToken.findById(params.emailToken).then(result => {
        console.log('ValidateEmailToken', {
            result
        })
        if (result) {
            response.status(200).json({
                success: true,
                email: result.email,
            })
        } else {
            response.status(200).json({
                success: false,
                error: 'resultIsNull',
            })
        }
    }).catch(error => {
        response.status(200).json({
            success: false,
            error,
        })
    })
}

async function ParseArguments(request, response) {
    console.log('ParseArguments')
    const emailToken = request.body.emailToken
    if (!emailToken) {
        response.status(200).json({
            error: 'missingArgument',
            detail: 'emailToken',
        })
    } else {
        ValidateEmailToken({
            emailToken
        }, response)
    }
}

router.post('', (request, response, next) => {
    ParseArguments(request, response)
})

module.exports = router
