const mongoose = require("mongoose")
const express = require("express")
const router = express.Router()

const rfr = require('rfr')

const objects = rfr("data/objects")
const login = rfr('middleware/login')
const createLabel = rfr('data/label/create')


async function StartCreateLabel(params, response) {
    console.log('StartCreateLabel', {
        params
    })
    // TODO(greg) Check the arguments.
    createLabel.LabelBroadcast(params.userId, params.broadcastId, params.objectType, params.labelName, params.labelValue).then(result => {
        console.log('StartCreateLabel', {
            result
        })
        response.status(200).json({
            success: true,
            result,
        })
    }).catch(error => {
        console.log('StartCreateLabel', {
            error
        })
        response.status(200).json({
            success: false,
            error,
        })
    })

}

async function ParseArguments(request, response) {
    const userId = request.authenticatedUser._id
    const objectType = 'broadcast' // hard-code for now
    const broadcastId = request.body.broadcastId
    const labelName = request.body.labelName
    const labelValue = request.body.labelValue
    if (!(userId && objectType && broadcastId && labelName && labelValue)) {
        response.status(200).json({
            success: false,
            originalBody: request.body,
        })
    } else {
        const params = {
            userId,
            broadcastId,
            objectType,
            labelName,
            labelValue,
        }
        StartCreateLabel(params, response)
    }
}

router.post("", login.Required, (request, response, next) => {
    ParseArguments(request, response)
})

module.exports = router
