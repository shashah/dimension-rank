const mongoose = require('mongoose')

const fs = require('fs')
const rfr = require('rfr')
const globalOptions = rfr('globalOptions')
const app = rfr('api/app')
const dataCassandra = rfr('data/cassandra')
const dataMongo = rfr('data/mongo')
const dataRedis = rfr('data/redis')

function LoadOptions(argv) {
    // Read in the secrets file.
    if (argv.length < 3) {
        console.log('LoadOptions', 'Argument 2 must be an options file name.')
        process.exit(1)
    } else {
        const optionsFname = argv[2]
        const fileText = fs.readFileSync(optionsFname)
        return JSON.parse(fileText)
    }
}

async function Main(options) {
    console.log('Main', {
        options
    })
		const loadResult = globalOptions.LoadOptions(options)
		console.log('Main', {loadResult})

    const cassandraResult = await dataCassandra.ResetCassandraClient(options.cassandra)
    console.log('Main', {
        cassandraResult,
    })

		const mongoResult = await dataMongo.ResetMongoClient(options.mongo)
    console.log('Main', {
        mongoResult,
    })

		const redisResult = await dataRedis.ResetRedisClient(options.redis)
    console.log('Main', {
        redisResult,
    })

    const port = options.port
    console.log('Main', {
        port,
    })
    app.listen(port, () => console.log(`Example app listening on port ${port}!`))
}

Main(LoadOptions(process.argv))
