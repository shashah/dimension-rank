// This file is for the *middleware* function that adds the user info to the
// request.

const rfr = require('rfr')

const objects = rfr('data/objects')
const cookieTokens = rfr('data/cookieToken/find')

function NotRequired(request, response, next) {
    next()
}

function Required(request, response, next) {
    console.log('Required', 'request.body', request.body)
    const cookieToken = request.body.cookieToken
    console.log('Required', 'cookieToken', cookieToken)

    // Short-circuit if cookieToken is null, i.e. don't hit DB.
    if (cookieToken == null) {
        response.status(200).json({
            success: false,
            error: 'loginFailure',
        })
        return
    } else {
        cookieTokens.FindCookieTokenUser(cookieToken).then(userPair => {
            console.log('Required', {
                userPair
            })
            if (userPair) {
                console.log('Required', 'userPair', userPair)
                request.authenticatedUser = userPair
                next()
            } else {
                response.status(200).json({
                    success: false,
                    error: 'loginFailure',
                })
                return
            }
        }).catch(error => {
            console.log('Required', 'error', error)
            response.status(200).json({
                success: false,
                error: 'loginFailure',
            })
        })
    }
}

module.exports = {
    Required,
    NotRequired,
}
