import cookies from '@/lib/cookies.js'
import logging from '@/lib/logging.js'
import network from '@/lib/network.js'

import renderResource from '@/components/renderResource.vue'
import renderReference from '@/components/renderReference.vue'

export default {
  name: 'renderBroadcast',
	props: {
		broadcast: Object,
		legend: String,
	},
	components: {
		renderResource,
		renderReference,
	},
  data () {
		return {
			showVoters: true,
		}
  },
	created() {
		console.log('renderBroadcast created')
	},
	methods: {
		Positive() {
			LabelAndNext(this, 1)
			this.showVoters = false
		},
		Negative() {
			LabelAndNext(this, 0)
			this.showVoters = false
		},
		Relay() {
			this.$router.push('/broadcast/create?reference=' + this.broadcast._id)
		},
		Permalink() {
			this.$router.push('/broadcast/read/' + this.broadcast._id)
		},
		ChannelTypeImpl() {
			const maybeChannel = this.broadcast.channelDescriptor
			if (!maybeChannel) {
				return 'user'
			} else {
				return maybeChannel.channelType
			}
		}
	},
	computed: {
		ShowName() {
			return this.ChannelTypeImpl() == 'open'
		},
		AuthorUserName() {
			return '@' + this.broadcast.producerTuple.userName
		},
		ChannelLink() {
			return 'mylink'
		},
		ChannelType() {
			return this.ChannelTypeImpl()
		},
		ChannelName() {
			const maybeChannel = this.broadcast.channelDescriptor
			if (!maybeChannel) {
				return 'u/' + this.broadcast.producerTuple.userName
			} else {
				if (maybeChannel.channelType == 'open') {
					return 'r/' + maybeChannel.channelName
				} else {
					return 'u/' + this.broadcast.producerTuple.userName
				}
			}
		},
		RenderTokens() {
			var buffer = ''
			// console.log('RenderTokens', 'this.broadcast', JSON.stringify(this.broadcast))
			for (const token of this.broadcast.tokens) {
				if (network.IsUrl(token)) {
					// pass
				} else {
					buffer += token + ' '
				}
			}
			return buffer
		},
	},
}

