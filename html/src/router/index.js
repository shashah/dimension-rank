import Vue from 'vue'
import VueRouter from 'vue-router'

import application_create from '../views/application/create.vue'
import application_verify from '../views/application/verify.vue'
import broadcast_create from '../views/broadcast/create.vue'
import broadcast_confirmCreate from '../views/broadcast/confirmCreate.vue'
import broadcast_delete from '../views/broadcast/delete.vue'
import broadcast_confirmDelete from '../views/broadcast/confirmDelete.vue'
import broadcast_relay from '../views/broadcast/relay.vue'
import broadcast_read from '../views/broadcast/read.vue'
import feed_single from '../views/feed/single.vue'
import index from '../views/index.vue'
import dataPolicy from '../views/dataPolicy.vue'
import invite_accept from '../views/invite/accept.vue'
import login_cookieToken from '../views/login/cookieToken.vue'
import login_emailToken from '../views/login/emailToken.vue'
import login_emailToken_success from '../views/login/emailToken_success.vue'
import login_logout from '../views/login/logout.vue'
import login_outside from '../views/login/outside.vue'
import login_applicationRequired from '../views/login/applicationRequired.vue'
import login_applicationSuccess from '../views/login/applicationSuccess.vue'
import user_create from '../views/user/create.vue'
import user_createBadToken from '../views/user/createBadToken.vue'
import user_creationSuccess from '../views/user/creationSuccess.vue'
import user_createProfile from '../views/user/createProfile.vue'
import user_edit from '../views/user/edit.vue'
import user_profile from '../views/user/profile.vue'
import user_update from '../views/user/update.vue'


Vue.use(VueRouter)

const routes = [
  { path: '/', name: 'login_emailToken', component: login_emailToken },

  { path: '/application/create', name: 'application_create', component: application_create },
  { path: '/application/verify', name: 'application_verify', component: application_verify },
  { path: '/broadcast/create', name: 'broadcast_create', component: broadcast_create },
  { path: '/broadcast/delete/:broadcastId', name: 'broadcast_delete', component: broadcast_delete },
  { path: '/broadcast/confirmDelete', name: 'broadcast_confirmDelete', component: broadcast_confirmDelete },
  { path: '/broadcast/confirmCreate/:broadcastId', name: 'broadcast_confirmCreate', component: broadcast_confirmCreate },
  { path: '/broadcast/relay/:referenceId', name: 'broadcast_relay', component: broadcast_relay },
  { path: '/broadcast/read/:broadcastId', name: 'broadcast_read', component: broadcast_read },
  { path: '/feed/single', name: 'feed_single', component: feed_single },
  { path: '/dataPolicy', name: 'dataPolicy', component: dataPolicy },
  { path: '/invitation/accept/:emailToken', name: 'invite_accept', component: invite_accept },
  { path: '/login/cookieToken/:emailToken', name: 'login_cookieToken', component: login_cookieToken },
  { path: '/login/emailToken', name: 'login_emailToken', component: login_emailToken },
  { path: '/login/emailToken_success/:email', name: 'login_emailToken_success', component: login_emailToken_success },
  { path: '/login/logout', name: 'login_logout', component: login_logout },
  { path: '/login/outside', name: 'login_outside', component: login_outside },
  { path: '/login/applicationRequired/:email', name: 'login_applicationRequired', component: login_applicationRequired },
  { path: '/login/applicationSuccess/:email', name: 'login_applicationSuccess', component: login_applicationSuccess },
  { path: '/user/create/:emailToken', name: 'user_create', component: user_create },
  { path: '/user/createBadToken', name: 'user_createBadToken', component: user_createBadToken },
  { path: '/user/creationSuccess', name: 'user_creationSuccess', component: user_creationSuccess },
  { path: '/user/createProfile/:userName', name: 'user_createProfile', component: user_createProfile },
  { path: '/user/edit', name: 'user_edit', component: user_edit },
  { path: '/user/profile/:userName', name: 'user_profile', component: user_profile },
  { path: '/user/update/:attribute', name: 'user_update', component: user_update },
]

const router = new VueRouter({
  mode: 'history',
  base: '/',
  routes
})

export default router
