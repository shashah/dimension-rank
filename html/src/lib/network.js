const axios = require('axios')
const querystring = require('querystring')

const cookies = require('./cookies')

function ValidateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}

async function NodeCall(fn_name, options) {
    const rest_base = process.env.VUE_APP_BASE_URL + '/api'
    const full_url = rest_base + fn_name
    console.log('NodeCall', full_url, fn_name, options)
    const result = await axios.post(full_url, querystring.stringify(options))
    return result
}

function GetCookie(cookie_name) {
    var nameEQ = cookie_name + '='
    var ca = document.cookie.split(';')
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i]
        while (c.charAt(0) == ' ') c = c.substring(1, c.length)
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length)
    }
    return null
}

async function CheckCookietokenCall() {
    const cookieToken = GetCookie('cookieToken')
    console.log('CheckCookietokenCall', {
        cookieToken
    })
    const options = {
        cookieToken
    }
    return NodeCall('/cookieToken/validate', options)
}

function FullImageUrl(image_path) {
    const image_base = process.env.VUE_APP_BASE_URL + '/images'
    return image_base + '/' + image_path
}

function IsUrl(string) {
    console.log('IsUrl', {
        string
    })
    var res = string.match(/(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/g);
    return (res !== null)
}

function HostName(url) {
    var hostname = (new URL(url)).hostname;
    return hostname
}

export default {
    ValidateEmail,
    NodeCall,
    CheckCookietokenCall,
    FullImageUrl,
    IsUrl,
    HostName,
}
