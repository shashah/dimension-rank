
import network from '@/lib/network.js'

function RenderResource(resource) {
	var buffer = ''
	if ('title' in resource) {
		// console.log('RenderTokens', 'title', resource.title)
		buffer += resource.title + '<br>'
	}
	if ('image' in resource) {
		// console.log('RenderTokens', 'image', resource.image)
		// const fullPath = network.FullImageUrl(resource.image.fileName)
		buffer += '<img src="' + resource.image + '"><br>'
	}
	return buffer
}

function RenderTokens(tokens) {
	var buffer = '<div class="render-tokens">'
	for (const token of tokens) {
		if (network.IsUrl(token)) {
			// Add nothing if it's a url for now.
			// was: buffer += network.HostName(part) + ' '
		} else {
			buffer += token + ' '
		}
	}
	buffer += '</div>'
	return buffer
}

export default {
    RenderResource,
    RenderTokens,
}
