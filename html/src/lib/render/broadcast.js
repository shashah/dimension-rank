import network from '@/lib/network.js'
import time from '@/lib/time.js'

import reference from '@/lib/render/reference.js'
import renderResource from '@/lib/render/resource.js'

function RenderTime(ms_time) {
	return time.UnixTimeAsString(ms_time) + '<br>'
}

function RenderUserName(userName) {
    var b = '<a href="/user/profile/' + userName + '">'
    b += userName
    b += '</a>'
    return b
}

function FullBroadcast (broadcast) {
	var buffer = ''
//	// Render the reference.
//	if (broadcast.renderingData.referenceInfo != null) {
//		buffer += reference.RenderReference(broadcast.renderingData.referenceInfo)
//	} else {
//		console.log("NOTE: no referenceInfo")
//	}
//	// Render user name.
//	buffer += user.RenderUserName(broadcast.renderingData.producerUserName)
//	buffer += RenderTime(broadcast.renderingData.timeCreated)
//	// Render textarea.
//	buffer += resource.RenderTextarea(broadcast.basicData.textarea)
//	// Render the resource.
//	if (broadcast.renderingData.resource != null) {
//		buffer += resource.RenderResource(broadcast.renderingData.resource)
//
//	}

	buffer += RenderUserName(broadcast.producerTuple.userName)
	buffer += renderResource.RenderTokens(broadcast.tokens)
	for (const resource of broadcast.resources) {
		buffer += renderResource.RenderResource(resource)
	}

	return buffer
}

export default {
	FullBroadcast
}

