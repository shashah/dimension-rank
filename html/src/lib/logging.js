function Log(caption, json) {
	console.log(caption + ': ' + JSON.stringify(json, null, 2))
}

export default {
	Log
}