function UnixTimeAsString(ms_time) {
    const options = {
        year: 'numeric', month: 'short', day: 'numeric',
        hour: 'numeric', minute: 'numeric', 
        hour12: false
    }

    const d = new Date(parseInt(ms_time) * 1000)
    return new Intl.DateTimeFormat('en-US', options).format(d)
}

export default {
    UnixTimeAsString
}
