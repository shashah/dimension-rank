import network from './network.js'
import cookies from './cookies.js'

// Creates a 'client-side storage key' for a broadcast.
// Returns 'String'.
function BroadcastKey(id) {
    return "Broadcast:" + id
}

async function FetchRemoteBroadcast(broadcastId) {}

// Return value: either 1) Promise<Broadcast> or 2) an error. Note: We have
// decided not to make the return value of a more complex type.
async function RetrieveBroadcast(broadcastId) {
    var cachedValue = null
		const broadcastKey = BroadcastKey(broadcastId)
    if (sessionStorage) {
        const cachedString = sessionStorage.getItem(broadcastKey)
        cachedValue = JSON.parse(cachedString)
    }

    if (cachedValue) {
        return new Promise(function(resolveSpecial, reject) {
            resolveSpecial(cachedValue)
        })
    } else {
        const cookieToken = cookies.GetCookie(cookies.authentication_token_cookie)
        const options = {
            cookieToken,
            broadcastId,
        }
        return new Promise(function(resolveSpecial, reject) {
            network.NodeCall('/broadcast/read', options)
                .then(result => {
                    console.log('RetrieveBroadcast', 'result', result)
                    if (result.data.success) {
                        const broadcast = result.data.broadcast
                        console.log('then', 'broadcastKey', broadcastKey)
                        sessionStorage.setItem(broadcastKey, JSON.stringify(broadcast))
                        console.log('then', 'resolveSpecial', resolveSpecial)
                        resolveSpecial(broadcast)
                    } else {
                        reject({
                            error: 'remoteFetchFailed'
                        })
                    }
                }).catch(error => {
                    reject(error)
                })
        })
    }
}

export default {
    RetrieveBroadcast,
}
