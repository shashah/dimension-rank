import cookies from '@/lib/cookies.js'
import network from '@/lib/network.js'
import render from '@/lib/render/broadcast.js'

import booleanMenuBar from '@/components/booleanMenuBar.vue'
import relayBar from '@/components/relayBar.vue'
import headerBar from '@/components/headerBar.vue'
import renderBroadcast from '@/components/renderBroadcast.vue'
import eventBus from '@/lib/eventBus.js'

function RelayBroadcast(instance, polarity) {
    const broadcastId = instance.broadcast._id
    const queryUrl = '/broadcast/create?referenceId=' + broadcastId + '&polarity=' + polarity
    instance.$router.push(queryUrl)
}

function UpdateHotnessLabel(instance, labelValue) {
    console.log('UpdateHotnessLabel', {
        labelValue
    })
    const cookieToken = cookies.GetCookie(cookies.authentication_token_cookie)
    const broadcastId = instance.broadcast._id
    const labelName = 'hot'
    const options = {
        cookieToken,
        broadcastId,
        labelName,
        labelValue,
    }
    network.NodeCall('/label/create', options)
        .then(result => {
            console.log('/label/create', 'result', result)
            if (result.data.success) {
                // pass
            } else if (result.data.error == 'loginFailure') {
                instance.$router.push('/login/outside')
            } else {
                console.log('/label/create', 'failed')
            }
        }).catch(error => {
            console.log('/label/create', 'error', error)
        })
}


function LoadRandomBroadcast(instance) {
    console.log('LoadRandomBroadcast')
    const cookieToken = cookies.GetCookie(cookies.authentication_token_cookie)
    const options = {
        cookieToken
    }
    network.NodeCall('/feed/single', options)
        .then(result => {
            console.log('/feed/single', 'result', result)

            if (result.data.success) {
                instance.broadcast = result.data.broadcast
            } else if (result.data.error == 'loginFailure') {
                instance.$router.push('/login/outside')
            } else {
                if (result.data.error && result.data.error.loginFailed) {
                    instance.$router.push('/login/outside')
                } else {
                    console.log('LoadRandomBroadcast', 'unhandled error', result.data)
                }
            }
        }).catch(error => {
            console.log('/feed/single', 'error', error)
        })
}

export default {
    name: 'prod_feed_single',
    components: {
        renderBroadcast,
        booleanMenuBar,
        relayBar,
        headerBar,
    },
    data() {
        return {
            broadcast: null,
        }
    },
    methods: {
        HandleHot() {
            console.log('HandleHot')
            UpdateHotnessLabel(this, true)
            LoadRandomBroadcast(this)
        },
        HandleCold() {
            console.log('HandleCold')
            UpdateHotnessLabel(this, false)
            LoadRandomBroadcast(this)
        },
        HandleRelay() {
            console.log('HandleRelay')
            RelayBroadcast(this, 'none')
        },
    },
    created() {
        console.log('prod_feed_single')
        LoadRandomBroadcast(this)
    },
    mounted() {},
}
