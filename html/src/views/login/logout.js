import cookies from '@/lib/cookies.js'
import logging from '@/lib/logging.js'
import network from '@/lib/network.js'
async function DestroyCookietoken(instance) {
	const cookieToken = cookies.GetCookie(cookies.authentication_token_cookie)
	const options = {
		cookieToken
	}
	console.log('DestroyCookietoken', {options})
	network.NodeCall('/cookieToken/delete', options)
	.then(result => {
		console.log('DestroyCookietoken', {result})
		if (result.data.success) {
			instance.$router.push('/login/outside')
		} else {
			instance.$router.push('/')
		}
	}, error => {
		console.log('DestroyCookietoken', {error})
	})
}

export default {
  name: 'prod_login_logout',
  data () {
		return {
		}
  },
	methods: {
	},
	created() {
		console.log('prod_login_logout')
		DestroyCookietoken(this)
	}
}

