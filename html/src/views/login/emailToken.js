import cookies from '@/lib/cookies.js'
import network from '@/lib/network.js'
async function CreateEmailToken(instance) {
	document.getElementById('EmailInputText').disabled = true
	document.getElementById('SubmitButton').disabled = true

	const email = instance.emailAddress
	console.log('CreateEmailToken', 'email', email)
	const options = {
		email,
	}
	network.NodeCall('/emailToken/create', options)
	.then(result => {
		console.log('/emailToken/create', 'result', result)
		if (result.data.success) {
				instance.$router.push('/login/emailToken_success/' + encodeURIComponent(email))
		} else {
			if (result.data.emailNotAuthorized) { 
				instance.$router.push('/login/applicationRequired/' + encodeURIComponent(email))
			} else {
				console.log('/emailToken/create', 'unexpected error', 'result', result)
			}
		}
	}).catch(error => {
		console.log('/emailToken/create', 'error', error)
	})
}

function GotoApplicationPage(instance) {
	const email = instance.listEmail
	instance.$router.push('/login/applicationRequired/' + encodeURIComponent(email))
}

async function CheckLoggedIn(instance) {
	const cookieToken = cookies.GetCookie(cookies.authentication_token_cookie)
	const options = {
		cookieToken
	}
	console.log('CheckLoggedIn', 'options', options)
	network.NodeCall('/cookieToken/validate', options)
	.then(result => {
		console.log('/cookieToken/validate', 'result', result)
		if (result.data.success == true) {
			instance.$router.push('/feed/single')
		}
	}).catch(error => {
		console.log('/cookieToken/validate', 'error', error)
	})
}

export default {
  name: 'prod_login_emailToken',
  data () {
		return {
			emailAddress: '',
			listName: '',
			listEmail: '',
			errorMessage1: '',
			errorMessage2: '',
			loginChannel: 'login',
		}
  },
	methods: {
		SubmitButton() {
			console.log('emailAddress', this.emailAddress)
			const emailIsValid = network.ValidateEmail(this.emailAddress)
			console.log('emailIsValid', emailIsValid)
			if (emailIsValid) {
				CreateEmailToken(this)
			} else {
				this.errorMessage1 = "Error: '" + this.emailAddress + "' is not a valid email address."
				document.getElementById("ErrorMessage1").classList.remove('Hidden')
				document.getElementById("ErrorMessage1").classList.add('Visible')
			}
		},
		EmailButton() {
			console.log('listEmail', this.listEmail)
			const emailIsValid = network.ValidateEmail(this.listEmail)
			console.log('emailIsValid', emailIsValid)
			if (emailIsValid) {
				GotoApplicationPage(this)
			} else {
				this.errorMessage2 = "Error: '" + this.listEmail + "' is not a valid email address."
				document.getElementById("ErrorMessage2").classList.remove('Hidden')
				document.getElementById("ErrorMessage2").classList.add('Visible')
			}
		},
		SetChannel(newType) {
			console.log({newType})
			this.loginChannel = newType
		},
		GetClass(className) {
			if (className == this.loginChannel) {
				return 'ChannelButton SelectedButton'
			} else {
				return 'ChannelButton UnselectedButton'
			}
		},
	},
	created() {
		console.log('prod_login_emailToken')
		if (this.emailAddress == null) {
			console.log('no emailAddress')
		}

		CheckLoggedIn(this)
	},
}

