import cookies from '@/lib/cookies.js'
import network from '@/lib/network.js'
import booleanMenuBar from '@/components/booleanMenuBar.vue'
import headerBar from '@/components/headerBar.vue'

async function ReadUser(instance, userName) {
    console.log('ReadUser', {userName})
    const cookieToken = cookies.GetCookie(cookies.authentication_token_cookie)
    const options = {
        cookieToken,
        userName,
    }
    network.NodeCall('/user/read', options)
        .then(result => {
            console.log('ReadUser', {result})
            instance.userView = Object.assign({}, result.data.userView)
            instance.resetUserView = Object.assign({}, instance.userView)
        }).catch(error => {
            console.log('ReadUser', 'error', error)
        })
}

async function UpdateUser(instance) {
    const cookieToken = cookies.GetCookie(cookies.authentication_token_cookie)
    const options = {
        cookieToken,
        description: instance.userView.description,
        profile: instance.userView.profile,
    }
    network.NodeCall('/user/update', options)
        .then(result => {
            console.log('UpdateUser', {
                result
            })
            instance.resetUserView = Object.assign({}, result.data.userView)
        }).catch(error => {
            console.log('UpdateUser', {
                error
            })
        })
}

export default {
    name: 'prod_user_profile',
    data() {
        return {
            userView: null,
            resetUserView: null,
        }
    },
    components: {
        booleanMenuBar,
        headerBar,
    },
    methods: {
        HandleSave() {
            console.log('HandleSave')
            UpdateUser(this)
        },
        HandleReset() {
            this.userView = this.resetUserView
        },
				CheckDataPolicy() {
					window.location.href = 'https://thinkdifferentagain.art/deep-revelations-data-policy.html'
				},
    },
    created() {
        console.log('prod_user_profile', 'this.$route.params', this.$route.params)
        if (this.$route.params && this.$route.params.userName) {
            ReadUser(this, this.$route.params.userName)
        } else {
            ReadUser(this, null)
        }
    }
}
