import cookies from '@/lib/cookies.js'
import network from '@/lib/network.js'
import dataInterface from '@/lib/dataInterface.js'
import render from '@/lib/render/broadcast.js'
import Vue from 'vue'
const getUrls = require('get-urls')

import renderBroadcast from '@/components/renderBroadcast.vue'
import channelSelector from '@/components/channelSelector.vue'
import resourceComponent from '@/components/resourceComponent.vue'
import booleanMenuBar from '@/components/booleanMenuBar.vue'
import headerBar from '@/components/headerBar.vue'

// Returns false if OK, or else an error string.
function CheckBroadcastTextarea(textarea) {
    const trimmed = textarea.trim()
    if (!trimmed) {
        console.log('CheckBroadcastTextarea', {
            trimmed
        })
        return 'Broadcast body is empty.'
    }
		return false
}


function UpdateReference(instance) {
    const referenceId = instance.reference_id()
    console.log('UpdateReference', {
        referenceId
    })

    dataInterface.RetrieveBroadcast(referenceId).then(function(broadcast) {
        instance.reference = broadcast
        console.log('UpdateReference', {
            broadcast
        })
    }).catch(function(error) {
        console.log('UpdateReference', 'error', error)
    })
}

// Returns false if OK, or else an error string.
function CheckChannel(channelDescriptor) {
    if (channelDescriptor.channelType == 'user') {
        return false
    } else {
        const channelName = channelDescriptor.channelName
        if (channelName.trim().length == 0) {
            return 'Network Channel name cannot be empty.'
        }
        var bannedCharacters = ''
        for (const c of channelName) {
            console.log({
                c
            })
            const letter1 = c >= 'A' && c <= 'Z'
            const letter2 = c >= 'a' && c <= 'z'
            const digit = c >= '0' && c <= '9'
            const special = c == '_'
            if (letter1 || letter2 || digit || special) {
                // pass
            } else {
                bannedCharacters += c
            }
        }

        if (bannedCharacters.length > 0) {
            return "The following characters are not allowed in a channel name: '" + bannedCharacters + "'"
        } else {
            return false
        }

    }
}

// Returns false if OK, or else an error string.
function CheckInputProblem(instance) {
    const channelProblem = CheckChannel(instance.channelDescriptor)
    console.log('CheckInputProblem', {
        channelProblem
    })
    if (channelProblem) {
        return channelProblem
    }

    const broadcastProblem = CheckBroadcastTextarea(instance.broadcastTextarea)
    console.log('CheckInputProblem', {
        broadcastProblem
    })
    if (broadcastProblem) {
        return broadcastProblem
    }

    return false
}


function TrySendBroadcast(instance) {
    const cookieToken = cookies.GetCookie(cookies.authentication_token_cookie)
    const options = {
        cookieToken,
        textarea: instance.broadcastTextarea,
        channelType: instance.channelDescriptor.channelType,
        channelName: instance.channelDescriptor.channelName,
        referenceId: instance.reference_id(),
    }
    console.log('TrySendBroadcast', {
        options
    })

    const inputProblem = CheckInputProblem(instance)
    console.log('TrySendBroadcast', {
        inputProblem
    })
    if (inputProblem) {
        console.log('TrySendBroadcast', 'Inputs Failed')
        // Note: assignment.
        instance.errorMessage = inputProblem
    } else {
        network.NodeCall('/broadcast/create', options)
            .then(result => {
                console.log('TrySendBroadcast', {
                    result
                })
                instance.$router.push('/broadcast/confirmCreate/' + result.data.broadcast._id)
            }).catch(error => {
                console.log('TrySendBroadcast', {
                    error
                })
            })
    }
}

function UpdateResource(instance, url) {
    const cookieToken = cookies.GetCookie(cookies.authentication_token_cookie)
    network.NodeCall('/resource/get', {
            cookieToken,
            url,
        })
        .then(result => {
            console.log('/resource/get', result)
            if (result.data.success) {
                instance.resource = result.data.resource
                console.log('UpdateResource', 'instance.resource', instance.resource)
            } else {
                console.log('/resource/get', 'failure')
            }
        }).catch(error => {
            console.log('/resource/get', error)
        })
}

export default {
    name: 'prod_broadcast_create',
    components: {
        renderBroadcast,
        channelSelector,
        resourceComponent,
        booleanMenuBar,
        headerBar,
    },
    data() {
        return {
            broadcastTextarea: '',
            reference: null,
            resource: null,
            channelDescriptor: {
                channelType: 'user'
            },
            errorMessage: null,
        }
    },
    methods: {
        reference_id() {
            return this.$route.query.referenceId
        },
        OnPaste() {},
        OnInput() {
            const urls = getUrls(this.broadcastTextarea)
            if (urls.size >= 1) {
                // Note: Only take first.
                for (const url of urls) {
                    UpdateResource(this, url)
                    break
                }
            }
        },
        HandleSend() {
            TrySendBroadcast(this)
        },
        HandleCancel() {
            // Note: re-assignment.
            this.broadcastTextarea = ''
        },
        HandleDescriptorChange(e) {
            console.log('create', {
                e
            })
            // Note: key line.
            this.channelDescriptor = e
            console.log('create', {
                t: this
            })
        },
    },
    computed: {
        BroadcastLegend() {
            if (this.reference_id()) {
                return "Repost a Revelation"
            } else {
                return "Post a Revelation"
            }
        },
        ShowError() {
            return this.errorMessage != null
        },
    },
    created() {
        UpdateReference(this)
    }
}
