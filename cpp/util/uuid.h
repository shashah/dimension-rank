#ifndef util_uuid_h
#define util_uuid_h 
#include "util/includes.h"

namespace dimensionrank {

Option<string> FreshTimeUUID();

} // namespace dimensionrank
#endif /* util_uuid_h */
