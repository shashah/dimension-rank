#ifndef util_secure_token_h
#define util_secure_token_h 
#include "util/includes.h"

namespace dimensionrank {

Option<string> FreshSecureToken();

} // namespace dimensionrank
#endif /* util_secure_token_h */
