#ifndef util_download_h
#define util_download_h 

#include "util/includes.h"

namespace dimensionrank {

Option<string> DownloadHttpPage(const string &url);

} // namespace dimensionrank
#endif /* util_download_h */
