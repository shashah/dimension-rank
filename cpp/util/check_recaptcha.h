#ifndef util_check_recaptcha_h
#define util_check_recaptcha_h 
#include "util/includes.h"
#include "curl/curl.h"
#include "util/smtp.pb.h"
#include "util/includes.h"
#include <iostream>
#include <stdio.h>
#include <string.h>
#include <string>

namespace dimensionrank {

Option<RecaptchaResponse> CheckRecaptcha(const RecaptchaRequest& request);

Boolean GetRecaptchaSuccess(const string& token);

} // namespace dimensionrank
#endif /* util_check_recaptcha_h */
