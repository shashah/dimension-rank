#include "util/secure_token.h"

#include "util/includes.h"
#include "util/random.h"

namespace dimensionrank {

Option<string> FreshSecureToken() {
	return Random62String(64);
}

} // namespace dimensionrank
