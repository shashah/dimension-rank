#include "util/includes.h"

#include <fstream>
#include <iostream>
#include <string>
#include <time.h>

namespace dimensionrank {

time_t CurrentTimeInfo() {
	return time(NULL);
}

}  // namespace dimensionrank
