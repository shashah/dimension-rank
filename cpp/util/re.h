#ifndef util_re_h
#define util_re_h 
#include "util/includes.h"

namespace dimensionrank {
namespace re {
vector<string> FindAllMatches(const string& haystack, const string& re);

}  // namespace re
}  // namespace dimensionrank
#endif /* util_re_h */
