#ifndef server_options_h
#define server_options_h 
#include "util/includes.h"
#include "server/options.pb.h"

namespace dimensionrank {

Option<ServerOptions> LoadServerOptions(const string& file_path);

} // namespace dimensionrank
#endif /* server_options_h */
