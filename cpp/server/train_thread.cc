#include "server/train_thread.h"

#include "data/cassandra_table.h"
#include "data/globals.h"
#include "neural/action_converter.h"
#include "neural/api.h"
#include "neural/eval.h"
#include "neural/network_interface.h"
#include "neural/recommendation_publisher.h"
#include "server/options.pb.h"
#include "util/includes.h"
#include "util/random.h"
#include <boost/filesystem.hpp>
#include <chrono>   // std::chrono::seconds
#include <iostream> // std::cout, std::endl
#include <memory>
#include <thread> // std::this_thread::sleep_for

#include <time.h>

using namespace std::chrono;
namespace dimensionrank {

Void TrainWorker::Init() { return Ok(); }

namespace {

Void RecordTrainAction(const BroadcastLabel &user_label,
                       const ClassLabel &class_label,
                       CassandraContext *cassandra_context) {
  TrainRecord record;
  record.set_user_id(user_label.subjectid());
  record.set_object_id(user_label.objectid());
  record.set_label_name(user_label.labelname());
  record.set_gold(user_label.labelvalue());
  record.set_prediction(class_label.value());
  record.set_details(class_label.DebugString());

  auto train_recorder = new TrainRecorder();
  ASSERT_SUCCEEDS(train_recorder->Record(record, cassandra_context));
  return Ok();
}

Void TrainSingle(const string &simpleString,
                 NetworkInterface *network_interface,
                 RedisInterface *redis_interface,
                 CassandraContext *cassandra_context) {
  BroadcastLabel label;
  ActionConverter converter;
  google::protobuf::util::JsonParseOptions options;
  options.ignore_unknown_fields = true;
  auto googleStatus = google::protobuf::util::JsonStringToMessage(
      simpleString, &label, options);
  ASSERT(googleStatus.ok());

  auto boolean_pair = converter.ConvertProblem(label);
  ASSERT_EXISTS(boolean_pair);

  auto network = network_interface->training_network();
  ASSERT_EXISTS(network);

  auto train_response = network->Train(
      boolean_pair->request(), boolean_pair->response(), redis_interface);
  ASSERT_EXISTS(train_response);

  ASSERT_SUCCEEDS(
      RecordTrainAction(label, train_response->label(), cassandra_context));

  return Ok();
}

bool EnoughTimePass(time_t *startTime, time_t currentTime, int numTime) {
  auto diff = currentTime - *startTime;
  if (diff > numTime) {
    *startTime = currentTime;
    return true;
  } else {
    return false;
  }
}

} // namespace

Void TrainWorker::Run(const ServerOptions &options,
                      NetworkInterface *network_interface) {

  auto cassandra_context = CreateCassandraContext(options.cassandra_options());
  ASSERT_EXISTS(cassandra_context);

  auto redis_context = CreateRedisContext(options.redis_options());
  ASSERT_EXISTS(redis_context);
  auto redis_interface = RedisInterface::Create(redis_context.get());
  ASSERT_EXISTS(redis_interface);

  auto feed = redis_interface->queueBroadcastLabels();

  time_t lastUpdateTime = time(NULL);
  while (true) {
    // Print_msg("train checking");
    auto action_string = feed->pop();
    ASSERT_OK(action_string);
    if (!action_string.has_value()) {
      // Print_msg("train sleeping");
      // If we don't have an action, sleep.
      std::this_thread::sleep_for(std::chrono::milliseconds(1000));
    } else {
      Print_msg("Have an action: " + *action_string);
      // If we have an action, do training.
      ASSERT_SUCCEEDS(TrainSingle(*action_string, network_interface,
                                  redis_interface.get(),
                                  cassandra_context.get()));

      // Just save every time.
      {
        auto network = network_interface->training_network();
        ASSERT_EXISTS(network);
        // TODO(greg) This shouldn't take the key as an argument.
        ASSERT_SUCCEEDS(network->Save(options.hot_model().model_key(),
                                      redis_interface.get()));
      }
    }
  }

  return Ok();
}

} // namespace dimensionrank
