#ifndef server_feed_thread_h
#define server_feed_thread_h 
#include "util/includes.h"
#include "server/options.pb.h"
#include "neural/network_interface.h"

namespace dimensionrank {

class TestWorker {
public:
	Void Init();
	Void Run(const ServerOptions& options,
                     NetworkInterface *network_interface);
};

} // namespace dimensionrank
#endif /* server_feed_thread_h */
