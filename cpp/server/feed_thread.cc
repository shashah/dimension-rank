#include "server/feed_thread.h"

#include "absl/flags/flag.h"
#include "absl/flags/parse.h"
#include "data/cassandra_table.h"
#include "data/connection.h"
#include "data/dimensionrank.pb.h"
#include "data/globals.h"
#include "data/mongo_table.h"
#include "data/redis_counter.h"
#include "neural/action_converter.h"
#include "neural/api.h"
#include "neural/network_interface.h"
#include "neural/recommendation_publisher.h"
#include "server/options.pb.h"
#include "util/includes.h"
#include "util/protos.h"
#include <boost/filesystem.hpp>
#include <chrono>   // std::chrono::seconds
#include <iostream> // std::cout, std::endl
#include <memory>
#include <thread> // std::this_thread::sleep_for

#include <bsoncxx/builder/stream/document.hpp>
#include <bsoncxx/json.hpp>
#include <mongocxx/client.hpp>
#include <mongocxx/instance.hpp>
#include <mongocxx/stdx.hpp>
#include <mongocxx/uri.hpp>

namespace dimensionrank {

using bsoncxx::type;
using bsoncxx::builder::stream::close_array;
using bsoncxx::builder::stream::close_document;
using bsoncxx::builder::stream::document;
using bsoncxx::builder::stream::finalize;
using bsoncxx::builder::stream::open_array;
using bsoncxx::builder::stream::open_document;
using bsoncxx::stdx::string_view;

namespace {

Option<BroadcastLabel> CreateProblem(const JsonBroadcast &broadcast,
                                     const string &subjectId) {
  BroadcastLabel result;
  result.set_subjectid(subjectId);
  result.set_authorid(broadcast.producertuple()._id());
  result.set_objectid(broadcast._id());
  result.set_labelname("hot");
  return result;
}

} // namespace

Void TestWorker::Init() {
  //  std::string uri_string = absl::GetFlag(FLAGS_uri);
  //  ASSERT(!uri_string.empty());
  //
  return Ok();
}

namespace {

// message RecommendationRecord {
//  optional string user_id = 1;
//  optional string object_id = 2;
//	optional string label_name = 3;
//  optional int32 prediction = 4;
//  optional string details = 5;
//}

Void RecordFeedAction(const string &user_id, const string &object_id,
                      bool recommended, const string &details,
                      CassandraContext *cassandra_context) {
  RecommendationRecord record;
  record.set_user_id(user_id);
  record.set_object_id(object_id);
  record.set_recommendation_made(recommended);
  record.set_details(details);

	Print_debug(record);

  auto recommendation_recorder = new RecommendationRecorder();
  ASSERT_SUCCEEDS(recommendation_recorder->Record(record, cassandra_context));
  return Ok();
}

Void ProcessBroadcastUser(const JsonBroadcast &broadcast,
                          bsoncxx::document::view *doc,
                          NetworkInterface *network_interface,
                          RedisInterface *redis_interface,
                          CassandraContext *cassandra_context) {
  auto oid_part = (*doc)["_id"];
  if (oid_part.type() != type::k_oid) {
    RAISE_EXCEPTION("This shouldn't happen.");
  }

  const auto oid = oid_part.get_oid();
  const string oid_string = oid.value.to_string();
  auto problem = CreateProblem(broadcast, oid_string);
  ASSERT_EXISTS(problem);
  ActionConverter converter;

  auto boolean_pair = converter.ConvertProblem(*problem);
  ASSERT_EXISTS(boolean_pair);

  auto network = network_interface->training_network();
  ASSERT_EXISTS(network);

  auto prediction = network->Predict(boolean_pair->request(), redis_interface);
  ASSERT_EXISTS(prediction);

  bool recommended = false;
  if (prediction->label().value()) {
    recommended = true;
    auto personal_queue = redis_interface->recommendation_queue(oid_string);
    auto json = ProtoToJsonString(broadcast);
    ASSERT_EXISTS(json);
    personal_queue->append(*json);
  }
	Print_debug(broadcast);
  ASSERT_SUCCEEDS(RecordFeedAction(oid_string, broadcast._id(), recommended,
                                   prediction->DebugString(),
                                   cassandra_context));
  return Ok();
}

Void ProcessBroadcast(const string &broadcast_string,
                      mongocxx::collection *users_collection,
                      NetworkInterface *network_interface,
                      RedisInterface *redis_interface,
                      CassandraContext *cassandra_context) {
  // Assume: broadcast_string.has_value()

  // This cast to a protocol buffer is being done because the input from Redis
  // is a JSON string.  This cast makes sense, in contrast to the cast from
  // Mongo might not be needed.
  JsonBroadcast broadcast;
  google::protobuf::util::JsonParseOptions options;
  options.ignore_unknown_fields = true;
	Print_value(broadcast_string);
  auto googleStatus = google::protobuf::util::JsonStringToMessage(
      broadcast_string, &broadcast, options);
  ASSERT(googleStatus.ok());

  // Big Loop:
  //
  // For each user u, decide if u "want to see" broadcast.
  mongocxx::cursor cursor = users_collection->find({});
  for (bsoncxx::document::view doc : cursor) {
    ASSERT_SUCCEEDS(ProcessBroadcastUser(broadcast, &doc, network_interface,
                                         redis_interface, cassandra_context));
  }
  return Ok();
}

} // namespace

Void TestWorker::Run(const ServerOptions &options,
                     NetworkInterface *network_interface) {

  auto mongo_context = CreateMongoContext(options.mongo_options());
  ASSERT_EXISTS(mongo_context);

  // TODO(greg) Guard this statement.
  auto users_collection = mongo_context->db["users"];

  auto redis_context = CreateRedisContext(options.redis_options());
  ASSERT_EXISTS(redis_context);

  auto redis_interface = RedisInterface::Create(redis_context.get());
  ASSERT_EXISTS(redis_interface);
  auto broadcast_feed = redis_interface->unsorted_broadcast_queue();

  auto cassandra_context = CreateCassandraContext(options.cassandra_options());
  ASSERT_EXISTS(cassandra_context);

  while (true) {
    auto broadcast_string = broadcast_feed->pop();
    ASSERT_OK(broadcast_string);

    // If nothing, sleep.
    if (!broadcast_string.has_value()) {
      std::this_thread::sleep_for(std::chrono::seconds(1));
    } else {
      ASSERT_SUCCEEDS(ProcessBroadcast(*broadcast_string, &users_collection,
                                       network_interface, redis_interface.get(),
                                       cassandra_context.get()));
    }
  }

  return Ok();
}

} // namespace dimensionrank
