#include "absl/flags/flag.h"
#include "absl/flags/parse.h"
#include "data/cassandra_table.h"
#include "data/connection.h"
#include "data/dimensionrank.pb.h"
#include "data/globals.h"
#include "data/mongo_table.h"
#include "neural/network_interface.h"
#include "server/feed_thread.h"
#include "server/options.h"
#include "server/options.pb.h"
#include "server/train_thread.h"
#include "util/includes.h"
#include "util/random.h"
#include <chrono>   // std::chrono::seconds
#include <iostream> // std::cout, std::endl
#include <memory>
#include <thread> // std::this_thread::sleep_for

ABSL_FLAG(std::string, options, "", "");

using namespace std;
using namespace dimensionrank;

void TrainRun(const ServerOptions &options,
              NetworkInterface *network_interface) {
  Output_msg("[TrainRun] starting");
  TrainWorker trainWorker;
  auto status = trainWorker.Run(options, network_interface);
  Output_value(status.status());
}

void TestRun(const ServerOptions &options,
             NetworkInterface *network_interface) {
  Output_msg("[TestRun] starting");
  TestWorker testWorker;
  auto status = testWorker.Run(options, network_interface);
  Output_value(status.status());
}

Void Main() {
  // Read the options.
  auto options = LoadServerOptions(absl::GetFlag(FLAGS_options));
  ASSERT_EXISTS(options);

  auto redis_context = CreateRedisContext(options->redis_options());
  ASSERT_EXISTS(redis_context);
  auto redis_interface = RedisInterface::Create(redis_context.get());
  ASSERT_EXISTS(redis_interface);

  // Initialize the network.
  auto network_interface =
      LoadNetworkInterface(options->hot_model(), redis_interface.get());
  ASSERT_EXISTS(network_interface);

  // Start both threads.
  std::thread t1(TrainRun, *options, network_interface.get());
  std::thread t2(TestRun, *options, network_interface.get());
  t1.join();
  t2.join();

  return Ok();
}

#include "util/main_incl.h"
