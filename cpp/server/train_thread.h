#ifndef server_train_thread_h
#define server_train_thread_h 
#include "util/includes.h"
#include "server/options.pb.h"
#include "neural/network_interface.h"

namespace dimensionrank {

class TrainWorker {
public:
	Void Init();
	Void Run(const ServerOptions& options, NetworkInterface* interface);
};

} // namespace dimensionrank
#endif /* server_train_thread_h */
