#include "server/options.h"

#include "util/includes.h"

namespace dimensionrank {

Option<ServerOptions> LoadServerOptions(const string& file_path) {
  auto spec = ReadProto<ServerOptions>(file_path);
  ASSERT_OK(spec);
  return *spec;
}

} // namespace dimensionrank
