#include "server/feed_thread.h"

#include "gmock/gmock.h"
#include "gtest/gtest.h"

namespace dimensionrank {

class TestWorker {
public:
	Void Init();
	void Run();
};

} // namespace dimensionrank
