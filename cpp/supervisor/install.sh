CONFIG=$1
if [ -z "${CONFIG}" ]; then
	echo "CONFIG not set"
	exit 1
fi

VERSIONED_FILE="${PWD}/supervisor/${CONFIG}.conf"
SUPERVISOR_FILE="/etc/supervisor/conf.d/cpp.${CONFIG}.conf"

echo VERSIONED_FILE [${VERSIONED_FILE}]
echo SUPERVISOR_FILE [${SUPERVISOR_FILE}]

sudo cp -f ${VERSIONED_FILE} ${SUPERVISOR_FILE}

