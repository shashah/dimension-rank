#include "data/api.h"

#include "util/includes.h"

namespace dimensionrank {
namespace keys {

StringKey FromString(const string &key) { return StringKey(key); }

} // namespace keys
} // namespace dimensionrank
