#ifndef data_globals_h
#define data_globals_h
#include "coln/map.h"
#include "data/api.h"
#include "data/cassandra_table.h"
#include "data/connection.h"
#include "data/map.h"
#include "data/mongo_table.h"
#include "data/seq.h"
#include "data/set.h"
#include "data/dimensionrank.pb.h"
#include "neural/training_config.pb.h"
#include "util/includes.h"
#include "util/smtp.pb.h"

// TODO(greg) None of these calls should fail.

namespace dimensionrank {
// Queues.
using StringMap = Map<string, string>;
using StringQueue = Queue<string>;
using StringSet = Set<string>;
using DoubleVectorMap = Map<string, DoubleVector>;

//
// REDIS

class RedisInterface {
public:
  static Unique<RedisInterface> Create(RedisContext *context);

  unique_ptr<StringQueue> unsorted_broadcast_queue();
  unique_ptr<StringQueue> unsorted_relay_queue();
  unique_ptr<StringQueue> queueBroadcastLabels();
  unique_ptr<StringQueue> unsorted_obscenity_queue();

  unique_ptr<StringQueue> recommendation_queue(const string &userId);

  unique_ptr<StringMap> network_paramaters(const string &model_key);
  unique_ptr<DoubleVectorMap> embedding_map(const string &embedding_group);

private:
  RedisInterface(RedisContext *context) : context_(context) {}
  RedisContext *context_;
};

////
//// CASSANDRA
// Unique<CassandraTable> recommendation_decision();
// Unique<CassandraTable> training_record();

//
// MONGO

} // namespace dimensionrank
#endif /* data_globals_h */
