#ifndef data_mongo_table_h
#define data_mongo_table_h 
#include "util/includes.h"
#include "server/options.pb.h"

#include <bsoncxx/builder/stream/document.hpp>
#include <bsoncxx/json.hpp>
#include <mongocxx/client.hpp>
#include <mongocxx/instance.hpp>
#include <mongocxx/stdx.hpp>
#include <mongocxx/uri.hpp>

namespace dimensionrank {

struct MongoContext {
  mongocxx::options::client client_options;
  mongocxx::options::tls tls_options;
  mongocxx::instance instance;
  unique_ptr<mongocxx::uri> uri;
  unique_ptr<mongocxx::client> client;
  mongocxx::database db;
};

Unique<MongoContext>
CreateMongoContext(const MongoOptions &options);

} // namespace dimensionrank
#endif /* data_mongo_table_h */
