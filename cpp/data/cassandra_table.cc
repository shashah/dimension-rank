#include "data/cassandra_table.h"

#include "util/includes.h"

namespace dimensionrank {

Unique<CassandraContext>
CreateCassandraContext(const CassandraOptions &options) {
Print_debug(options);
  auto result = new CassandraContext();
  result->cluster = cass_cluster_new();
  result->session = cass_session_new();
//  cass_cluster_set_protocol_version(result->cluster, 3);

  // Astra Step 1: Load the bundle.
  const char *secure_connect_bundle = options.secure_connect_bundle().c_str();
  if (cass_cluster_set_cloud_secure_connection_bundle(
          result->cluster, secure_connect_bundle) != CASS_OK) {
    RAISE_EXCEPTION("Could not open secure bundle");
  }

  // Astra Step 2: Set Credientials.
  cass_cluster_set_credentials(result->cluster, options.username().c_str(),
                               options.password().c_str());

  //  cass_cluster_set_contact_points(result->cluster, options.uri().c_str());

  result->connect_future =
      cass_session_connect(result->session, result->cluster);
  // result->keyspace = options.keyspace();

  if (cass_future_error_code(result->connect_future) == CASS_OK) {
    return result;
  } else {
    delete result;
    RAISE_EXCEPTION("Problem with the Cassandra connection.");
  }
}

Void RecommendationRecorder::Record(const RecommendationRecord &record,
                                    CassandraContext *context) {
  const string query =
      StrCat("INSERT INTO ", context->keyspace, ".recommendation_feed_action",
             "(id, userid, objectid, recommendation_made, details)",
             " values(now(), ?, ?, ?, ?)");
  CassStatement *statement = cass_statement_new(query.c_str(), 4);
  cass_statement_bind_string(statement, 0, record.user_id().c_str());
  cass_statement_bind_string(statement, 1, record.object_id().c_str());
  cass_bool_t recommendation_made =
      static_cast<int>(record.recommendation_made());
  cass_statement_bind_bool(statement, 2, recommendation_made);
  cass_statement_bind_string(statement, 3, record.details().c_str());

  CassFuture *result_future = cass_session_execute(context->session, statement);
  ASSERT(cass_future_error_code(result_future) == CASS_OK);
  return Ok();
}

Void TrainRecorder::Record(const TrainRecord &record,
                           CassandraContext *context) {
  const string query =
      StrCat("INSERT INTO ", context->keyspace, ".label_train_action",
             "(id, userid, objectid, labelname, gold, prediction, "
             "details)",
             " values(now(), ?, ?, ?, ?, ?, ?)");
  CassStatement *statement = cass_statement_new(query.c_str(), 6);
  cass_statement_bind_string(statement, 0, record.user_id().c_str());
  cass_statement_bind_string(statement, 1, record.object_id().c_str());
  cass_statement_bind_string(statement, 2, record.label_name().c_str());
  cass_statement_bind_int32(statement, 3, record.gold());
  cass_statement_bind_int32(statement, 4, record.prediction());
  cass_statement_bind_string(statement, 5, record.details().c_str());

  CassFuture *result_future = cass_session_execute(context->session, statement);
  ASSERT(cass_future_error_code(result_future) == CASS_OK);
  return Ok();
}

} // namespace dimensionrank
