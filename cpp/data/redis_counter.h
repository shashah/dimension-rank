#ifndef data_redis_counter_h
#define data_redis_counter_h
#include "util/includes.h"
#include "data/connection.h"

namespace dimensionrank {

// Wraps a redis counter. Can be used for logging liveness without using a lot
// of disk or log space.
class RedisCounter {
  RedisCounter(const string &key, RedisContext* redis) : key_(key), redis_(redis) {}

  Void Increment();

private:
  string key_;
	RedisContext* redis_;
};

} // namespace dimensionrank
#endif /* data_redis_counter_h */
