load("@rules_cc//cc:defs.bzl", "cc_binary", "cc_library", "cc_test")
load("//:defs.bzl", "dimensionrank_copts", "dimensionrank_linkopts")

package(default_visibility = ["//visibility:public"])

cc_proto_library(
    name = "redis_cc",
    deps = ["redis"],
)

proto_library(
    name = "redis",
    srcs = ["redis.proto"],
)

# DEPRECATED
cc_proto_library(
    name = "gold_cc",
    deps = ["dimensionrank"],
)

proto_library(
    name = "dimensionrank",
    srcs = ["dimensionrank.proto"],
)

cc_library(
    name = "connection",
    srcs = ["connection.cc"],
    hdrs = ["connection.h"],
    copts = dimensionrank_copts,
    linkopts = dimensionrank_linkopts,
    deps = [
        ":redis_cc",
        "//server:options",
        "//util:includes",
        "@com_google_absl//absl/strings",
        "@com_google_absl//absl/types:optional",
        "@com_google_protobuf//:protobuf_lite",
        "@hiredis",
    ],
)

cc_test(
    name = "connection_test",
    size = "small",
    srcs = ["connection_test.cc"],
    copts = dimensionrank_copts,
    linkopts = dimensionrank_linkopts,
    deps = [
        ":connection",
        ":redis_cc",
        "@com_google_googletest//:gtest_main",
        "@hiredis",
    ],
)

cc_test(
    name = "globals_test",
    srcs = ["globals_test.cc"],
    copts = dimensionrank_copts,
    linkopts = dimensionrank_linkopts,
    deps = [
        ":globals",
        "//util:includes",
        "@com_google_googletest//:gtest_main",
    ],
)

cc_test(
    name = "map_test",
    srcs = ["map_test.cc"],
    copts = dimensionrank_copts,
    linkopts = dimensionrank_linkopts,
    deps = [
        ":gold_cc",
        ":map",
        "//util:includes",
        "@com_google_googletest//:gtest_main",
    ],
)

cc_test(
    name = "seq_test",
    srcs = ["seq_test.cc"],
    copts = dimensionrank_copts,
    linkopts = dimensionrank_linkopts,
    deps = [
        ":seq",
        "//util:includes",
        "@com_google_googletest//:gtest_main",
    ],
)

cc_test(
    name = "set_test",
    srcs = ["set_test.cc"],
    copts = dimensionrank_copts,
    linkopts = dimensionrank_linkopts,
    deps = [
        ":set",
        "//util:includes",
        "@com_google_googletest//:gtest_main",
    ],
)

cc_test(
    name = "converter_test",
    srcs = ["converter_test.cc"],
    copts = dimensionrank_copts,
    linkopts = dimensionrank_linkopts,
    deps = [
        ":converter",
        "//data:gold_cc",
        "//util:includes",
        "@com_google_googletest//:gtest_main",
    ],
)

cc_test(
    name = "api_test",
    srcs = ["api_test.cc"],
    copts = dimensionrank_copts,
    linkopts = dimensionrank_linkopts,
    deps = [
        ":api",
        "//util:includes",
        "@com_google_googletest//:gtest_main",
    ],
)

cc_test(
    name = "cassandra_table_test",
    srcs = ["cassandra_table_test.cc"],
    deps = [
        ":cassandra_table",
        "//util:includes",
        "@com_google_googletest//:gtest_main",
    ],
)

cc_test(
    name = "mongo_table_test",
    srcs = ["mongo_table_test.cc"],
    deps = [
        ":mongo_table",
        "//util:includes",
        "@com_google_googletest//:gtest_main",
    ],
)

cc_test(
    name = "redis_counter_test",
    srcs = ["redis_counter_test.cc"],
    deps = [
        ":redis_counter",
        "//util:includes",
        "@com_google_googletest//:gtest_main",
    ],
)

cc_library(
    name = "globals",
    srcs = ["globals.cc"],
    hdrs = ["globals.h"],
    copts = dimensionrank_copts,
    linkopts = dimensionrank_linkopts,
    deps = [
        ":api",
        ":cassandra_table",
        ":connection",
        ":gold_cc",
        ":map",
        ":mongo_table",
        ":seq",
        ":set",
        "//coln:map",
        "//neural:training_config_cc",
        "//util:includes",
        "//util:smtp_cc",
    ],
)

cc_library(
    name = "map",
    srcs = ["map.cc"],
    hdrs = ["map.h"],
    copts = dimensionrank_copts,
    linkopts = dimensionrank_linkopts,
    deps = [
        ":connection",
        ":converter",
        "//coln:api",
        "//coln:map",
        "//util:includes",
    ],
)

cc_library(
    name = "seq",
    srcs = ["seq.cc"],
    hdrs = ["seq.h"],
    copts = dimensionrank_copts,
    linkopts = dimensionrank_linkopts,
    deps = [
        ":connection",
        ":converter",
        "//coln:api",
        "//coln:seq",
        "//util:includes",
    ],
)

cc_library(
    name = "set",
    srcs = ["set.cc"],
    hdrs = ["set.h"],
    copts = dimensionrank_copts,
    linkopts = dimensionrank_linkopts,
    deps = [
        ":connection",
        "//coln:api",
        "//coln:set",
        "//util:includes",
    ],
)

cc_library(
    name = "converter",
    srcs = ["converter.cc"],
    hdrs = ["converter.h"],
    copts = dimensionrank_copts,
    linkopts = dimensionrank_linkopts,
    deps = ["//util:includes"],
)

cc_library(
    name = "api",
    srcs = ["api.cc"],
    hdrs = ["api.h"],
    copts = dimensionrank_copts,
    linkopts = dimensionrank_linkopts,
    deps = ["//util:includes"],
)

cc_library(
    name = "cassandra_table",
    srcs = ["cassandra_table.cc"],
    hdrs = ["cassandra_table.h"],
    deps = [
        "//server:options",
        "//util:includes",
				"@cassandra",
    ],
    copts = dimensionrank_copts,
    linkopts = dimensionrank_linkopts,
)

cc_library(
    name = "mongo_table",
    srcs = ["mongo_table.cc"],
    hdrs = ["mongo_table.h"],
    deps = [
        "//server:options",
        "//util:includes",
    ],
    copts = dimensionrank_copts,
    linkopts = dimensionrank_linkopts,
)

cc_library(
    name = "redis_counter",
    srcs = ["redis_counter.cc"],
    hdrs = ["redis_counter.h"],
    deps = [
		"//util:includes",
		"//data:connection",
		],
    copts = dimensionrank_copts,
    linkopts = dimensionrank_linkopts,
)
