#include "data/redis_counter.h"

#include "data/connection.h"
#include "util/includes.h"

namespace dimensionrank {

Void RedisCounter::Increment() {
  auto redis_response = redis_->Execute({"INCR %s", key_});
  ASSERT_OK(redis_response);
  return Ok();
}

} // namespace dimensionrank
