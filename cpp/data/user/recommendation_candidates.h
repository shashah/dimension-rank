#ifndef data_user_recommendation_candidates_h
#define data_user_recommendation_candidates_h 
#include "util/includes.h"

namespace dimensionrank {

Option<vector<string>> GetRecommendtationCandidates();

} // namespace dimensionrank
#endif /* data_user_recommendation_candidates_h */
