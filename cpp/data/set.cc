#include "data/set.h"

#include "util/includes.h"

namespace dimensionrank {
namespace data {

Unique<Set<string>> RedisSet(const string &key, RedisContext* redis) {
  return unique_ptr<Set<string>>(new impl::RedisSet(key, redis));
}

Option<string> RedisRandomSetElement(const string& set_key, RedisContext* redis) {
	auto result = redis->Execute({"SRANDMEMBER %s", set_key});
	ASSERT_OK(result);
	return result->string_value();
}

} // namespace data
} // namespace dimensionrank
