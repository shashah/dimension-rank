#include "data/globals.h"

#include "util/includes.h"

namespace dimensionrank {

using data::RedisStringMap;
using data::RedisStringQueue;

Unique<RedisInterface> RedisInterface::Create(RedisContext *context) {
  ASSERT(context != nullptr);
  return new RedisInterface(context);
}

unique_ptr<StringQueue> RedisInterface::unsorted_broadcast_queue() {
  return RedisStringQueue("queueUnsortedBroadcasts", context_);
}
unique_ptr<StringQueue> RedisInterface::unsorted_relay_queue() {
  return RedisStringQueue("unsorted_relay_queue", context_);
}
unique_ptr<StringQueue> RedisInterface::queueBroadcastLabels() {
  return RedisStringQueue("queueBroadcastLabels", context_);
}
unique_ptr<StringQueue> RedisInterface::unsorted_obscenity_queue() {
  return RedisStringQueue("unsorted_obscenity_queue", context_);
}

unique_ptr<StringQueue>
RedisInterface::recommendation_queue(const string &userId) {
  return RedisStringQueue(StrCat("personal:", userId), context_);
}

unique_ptr<StringMap>
RedisInterface::network_paramaters(const string &model_key) {
  return RedisStringMap(StrCat("network_parameters:", model_key), context_);
}

unique_ptr<DoubleVectorMap>
RedisInterface::embedding_map(const string &embedding_group) {
  return data::RedisProtoMap<DoubleVector>(
      StrCat("embedding_map:", embedding_group), context_);
}

} // namespace dimensionrank
