#ifndef data_set_h
#define data_set_h 
#include "coln/api.h"
#include "coln/set.h"
#include "data/connection.h"
#include "util/includes.h"

namespace dimensionrank {
namespace data {
namespace impl {
// TODO(greg) This is the same as RedisSeqIterator.
class RedisSetIterator : public ConstIterator<string> {
public:
  RedisSetIterator(RedisResponse &&underlying)
      : underlying_(std::move(underlying)) {}
  Const<string> current() const override {
		return Const<string>(new string(underlying_.child(idx_).string_value()));
  }
  bool done() { 
		return idx_ == underlying_.child_size();
	}
  void advance() { ++idx_; }

private:
  RedisResponse underlying_;
  int idx_ = 0;
};

class RedisSet : public Set<string> {
public:
  RedisSet(const string &key, RedisContext* redis) : key_(key), redis_(redis) {}
  Void erase(const string &k) override {
		RAISE_EXCEPTION("not implemented");
	}
  Boolean contains(const string &k) const override {
    auto response = redis_->Execute({"SISMEMBER %s %s", key_, k});
		ASSERT_OK(response);
    return response->bool_value();
	}
	Size size() const override {
    RedisResponse redis_response;
    auto response = redis_->Execute({"SCARD %s", key_});
		ASSERT_OK(response);
    return response->int_value();
	}
	Void insert(const string&value) override {
		RedisResponse redis_response;
		auto status = redis_->Execute({"SADD %s %s", key_, value});
		ASSERT_OK(status);
		return Ok();
	}
	Void insert(string&&t) override {
		auto status = insert(t);
		ASSERT_OK(status);
		return status;
	}
	Shared<ConstIterator<string>> iterator() const override {
    RedisResponse redis_response;
    auto response = redis_->Execute({"SMEMBERS %s", key_});
		return new RedisSetIterator(std::move(*response));
	}
private:
  string key_;
  unordered_map<string, string> under_;
	RedisContext *redis_;
};

} // namespace impl

Unique<Set<string>> RedisSet(const string &key, RedisContext* redis) ;

Option<string> RedisRandomSetElement(const string& set_key, RedisContext* redis);

} // namespace data
} // namespace dimensionrank
#endif /* data_set_h */
