#include "data/seq.h"

#include "util/includes.h"

namespace dimensionrank {
namespace data {

unique_ptr<Queue<string>> RedisStringQueue(const string &key, RedisContext* redis) {
  return unique_ptr<Queue<string>>(new impl::RedisStringQueue(key, redis));
}
}
} // namespace dimensionrank
