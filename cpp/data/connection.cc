#include "data/connection.h"
#include <stdlib.h>
#include <string.h>

#include "absl/memory/memory.h"
#include "absl/strings/str_cat.h"
#include "absl/strings/str_split.h"
#include "absl/types/optional.h"
#include "hiredis.h"
#include "server/options.h"
#include <google/protobuf/stubs/status_macros.h>
#include <google/protobuf/stubs/statusor.h>

namespace dimensionrank {

namespace {

RedisResponse ConvertResponse(const redisReply &native) {
  RedisResponse result;
  result.set_error_code(native.type);
  switch (native.type) {
  case 1:
    // String.
		// Note: Binary-safe conversion.
    string r(native.str, native.len);
    result.set_string_value(r);
    break;
  case 2:
    // Array.
    for (size_t i = 0; i < native.elements; ++i) {
      redisReply *rx = native.element[i];
      *(result.add_child()) = ConvertResponse(*rx);
    }
    break;
  case 3:
    // Integer.
    result.set_int_value(native.integer);
    break;
  default:
    break;
  }
  return result;
}

Void RedisContext::Connect(const string &host_name, int port) {
  const struct timeval timeout = {1, 500000}; // 1.5 seconds
  context_ = nullptr;
  context_ = redisConnectWithTimeout(host_name.c_str(), port, timeout);
  if (context_ == nullptr) {
    RAISE_EXCEPTION("RedisContext is null.");
  } else if (context_->err) {
    redisFree(context_);
    RAISE_EXCEPTION("RedisContext is error.");
  } else {
    return Ok();
  }
}

Unique<RedisResponse>
RedisContext::Execute(const vector<string> &query) {
  redisReply *reply;
  if (query.size() == 1) {
    reply = redisCommand(context_, query[0].c_str());
  } else if (query.size() == 2) {
    reply = redisCommand(context_, query[0].c_str(), query[1].c_str());
  } else if (query.size() == 3) {
    reply = redisCommand(context_, query[0].c_str(), query[1].c_str(),
                         query[2].c_str());
  } else if (query.size() == 4) {
    reply = redisCommand(context_, query[0].c_str(), query[1].c_str(),
                         query[2].c_str(), query[3].c_str());
  } else {
    RAISE_EXCEPTION("This many arguments are not implemented.");
  }

  RedisResponse result = ConvertResponse(*reply);
  freeReplyObject(reply);
  return new RedisResponse(result);
}

Unique<RedisResponse>
RedisContext::ExecuteBinaryMapPut(const string &command, const string &map_name,
                                  const string &key,
                                  const string &value) {
  redisReply *reply;
  const char *c = value.c_str();
  reply = redisCommand(context_, command.c_str(), map_name.c_str(), key.c_str(),
                       c, value.size());
  RedisResponse result = ConvertResponse(*reply);
  freeReplyObject(reply);
  return new RedisResponse(result);
}

} // namespace


Unique<RedisContext> CreateRedisContext(const RedisOptions &options) {
  auto result = new RedisContext();
  ASSERT_SUCCEEDS(result->Connect(options.uri(), 6379));
  return result;
}

} // namespace dimensionrank
