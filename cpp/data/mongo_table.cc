#include "data/mongo_table.h"

#include "util/includes.h"
#include <stdio.h>

#include <iostream>

#include <bsoncxx/builder/stream/document.hpp>
#include <bsoncxx/json.hpp>
#include <mongocxx/client.hpp>
#include <mongocxx/instance.hpp>
#include <mongocxx/stdx.hpp>
#include <mongocxx/uri.hpp>

using bsoncxx::builder::stream::close_array;
using bsoncxx::builder::stream::close_document;
using bsoncxx::builder::stream::document;
using bsoncxx::builder::stream::finalize;
using bsoncxx::builder::stream::open_array;
using bsoncxx::builder::stream::open_document;
using bsoncxx::stdx::string_view;

namespace dimensionrank {

Unique<MongoContext>
CreateMongoContext(const MongoOptions &options) {
  auto result = new MongoContext();
  result->uri.reset(new mongocxx::uri(options.uri()));
  result->client.reset(new mongocxx::client(*result->uri));
  result->db = (*result->client)[options.database()];
  return result;
}

} // namespace dimensionrank
