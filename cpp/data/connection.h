#ifndef REDIS_CONNECTION
#define REDIS_CONNECTION
#include "data/redis.pb.h"
#include "hiredis.h"
#include "server/options.pb.h"
#include "util/includes.h"
namespace dimensionrank {

// This is an abstract connection so that it can be mocked out for test.
class RedisContext {
public:
Void Connect(const string &host_name, int port);
  // Executes 'query' against the underlying databse.
  Unique<RedisResponse> Execute(const vector<string> &query);

  // Special case for "binary map put". Can refactor later.
  Unique<RedisResponse> ExecuteBinaryMapPut(const string &command,
                                            const string &map_name,
                                            const string &key,
                                            const string &value);

private:
  // POINTER: Freed in Disconnect() using legacy c-language command.
  redisContext *context_ = nullptr;
};

// Use the host name in "server options".
Unique<RedisContext> CreateRedisContext(const RedisOptions &options);

} // namespace dimensionrank
#endif /* REDIS_CONNECTION */
