#ifndef data_cassandra_table_h
#define data_cassandra_table_h
#include "cassandra.h"
#include "server/options.pb.h"
#include "util/includes.h"

namespace dimensionrank {

struct CassandraContext {
  CassCluster *cluster = nullptr;
  CassSession *session = nullptr;
  CassFuture *connect_future = nullptr;

  ~CassandraContext() {
    cass_future_free(connect_future);
    cass_session_free(session);
    cass_cluster_free(cluster);
  }

  string keyspace;
};

Unique<CassandraContext>
CreateCassandraContext(const CassandraOptions &options);

template <typename OutputType> class CassandraRecorder {
  virtual Void Record(const OutputType &proto, CassandraContext *context) = 0;
};

class RecommendationRecorder : public CassandraRecorder<RecommendationRecord> {
public:
  Void Record(const RecommendationRecord &record,
              CassandraContext *context) override;
};

class TrainRecorder : public CassandraRecorder<TrainRecord> {
public:
  Void Record(const TrainRecord &record, CassandraContext *context) override;
};

} // namespace dimensionrank
#endif /* data_cassandra_table_h */
