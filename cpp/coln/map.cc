#include "coln/map.h"

namespace dimensionrank {

std::function<int()> EmptyInt() {
	return []() { return 0; };
}

std::function<float()> EmptyFloat() {
	return []() { return 0.0; };
}

} // namespace dimensionrank
