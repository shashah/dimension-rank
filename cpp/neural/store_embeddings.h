#ifndef neural_store_embeddings_h
#define neural_store_embeddings_h
#include "coln/api.h"
#include "data/dimensionrank.pb.h"
#include "data/globals.h"
#include "neural/features.pb.h"
#include "neural/training_config.pb.h"
#include "torch/torch.h"
#include "util/includes.h"

namespace dimensionrank {

using torch::Tensor;
using StringTensorMap = unordered_map<string, Tensor>;

// Create a proto buffer from a tensor.
DoubleVector VectorFromTensor(const Tensor &tensor);

// Will create a new tensor in the DB if not found.
Shared<Tensor> LoadEmbeddingTensor(const Feature &feature, size_t dimension,
                                   RedisInterface *redis_interface);

void SaveEmbeddingTensors(const RepeatedPtrField<Feature> &feature,
                          const StringTensorMap &tensor,
                          RedisInterface *redis_interface);

Void ResetEmbedding(const string &embedding_group, const string &value,
                    size_t dimension, RedisInterface *redis_interface);

Option<size_t> EmbeddingSize(const NetworkSpec &model_spec,
                             const string &embedding_group);

Option<size_t> EmbeddingConcatSize(const NetworkSpec &model_spec);

} // namespace dimensionrank
#endif /* neural_store_embeddings_h */
