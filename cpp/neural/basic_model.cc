#include "neural/basic_model.h"

#include "data/globals.h"
#include "neural/embedding_layer.h"
#include "neural/store_embeddings.h"
#include "util/includes.h"
#include <math.h>
#include <sstream>

namespace dimensionrank {
namespace impl {
namespace {

using StringTensorMap = StdMap<string, Tensor>;

struct LocalParameters {
  torch::nn::Linear linear1 = nullptr;
  torch::nn::Linear linear2 = nullptr;
};

class EmbeddingNetwork : public torch::nn::Module {
public:
  Void Init() {
    register_module("embedding_layer", embedding_layer_);
    register_module("linear1", locals_.linear1);
    register_module("linear2", locals_.linear2);
		return Ok();
  }
  torch::Tensor forward(torch::Tensor x) {
    auto concat = embedding_layer_->forward(x);
    auto e = locals_.linear1->forward(concat);
    auto f = torch::relu(e);
    auto g = locals_.linear2->forward(f);
    return torch::log_softmax(g, 0);
  }

public:
  LocalParameters locals_;
  std::shared_ptr<EmbeddingLayer> embedding_layer_;
};

Void NormalizeLogits(ClassLabel *response) {
  vector<float> parts;
  float sum = 0.0;
  for (size_t i = 0; i < response->activation().size(); ++i) {
    const float part = exp(response->activation(i));
    parts.push_back(part);
    sum += part;
  }
  for (size_t i = 0; i < response->activation().size(); ++i) {
    response->add_probability(parts[i] / sum);
  }
  return Ok();
}

} // namespace

class BasicModel : public ForwardBackwardModel {
public:
  LocalParameters locals_;
  unique_ptr<torch::optim::SGD> optimizer_;
  NetworkSpec network_spec_;

private:
  int output_dimension() { return network_spec_.output_space().dimension(); }

public:
  Unique<ForwardBundle>
  Forward(const DecisionRequest &request,
          RedisInterface *redis_interface) const override {
								Print_msg(".");
    EmbeddingNetwork network;
								Print_msg(".");
    network.locals_ = locals_;
								Print_msg(".");
    auto layer = EmbeddingLayer::CreateFrom(request.feature(), redis_interface);
								Print_msg(".");
    ASSERT_EXISTS(layer);
								Print_msg(".");
    network.embedding_layer_ = layer.expose();
								Print_msg(".");
    ASSERT_SUCCEEDS(network.Init());
								Print_msg(".");

    auto bundle = new ForwardBundle();
								Print_msg(".");
    bundle->output = network.forward(torch::empty({0}));
								Print_msg(".");
    bundle->module_state = network;
								Print_msg(".");
    return bundle;
  }

  Unique<DecisionResponse>
  ResponseFromActivation(const Tensor &tensor) const override {
    // Note: Boolean response is hard-coded.
    auto [idx, jdx] = torch::max(tensor, 0);
    auto response = new DecisionResponse();
    auto *label = response->mutable_label();
    label->set_value(jdx.item<int>());
    for (size_t i = 0; i < output_dimension(); ++i) {
      label->add_activation(tensor[i].template item<float>());
    }
    ASSERT_SUCCEEDS(NormalizeLogits(label));
    return response;
  }
  Void Backward(const ForwardBundle &bundle,
                const DecisionResponse &gold_response,
                RedisInterface *redis_interface) override {
								Print_msg(".");
    optimizer_.reset(new torch::optim::SGD(bundle.module_state.parameters(),
                                           torch::optim::SGDOptions(0.1)));
								Print_msg(".");
    optimizer_->zero_grad();
								Print_msg(".");

    int goldDecision = gold_response.label().value();
								Print_msg(".");
    Print_value(output_dimension());
								Print_msg(".");
    Print_value(goldDecision);
								Print_msg(".");
    ASSERT(goldDecision < output_dimension());
								Print_msg(".");
    vector<long> buffer;
								Print_msg(".");
    buffer.push_back(goldDecision);
								Print_msg(".");
    Tensor target = torch::tensor(buffer);
								Print_msg(".");
    torch::Tensor loss = torch::nll_loss(bundle.output, target);
								Print_msg(".");
    loss.backward();
								Print_msg(".");
    optimizer_->step();
								Print_msg(".");
    return Ok();
  }

  Void Load(const string &model_key, RedisInterface *redis_interface) override {
    auto parameter_map = redis_interface->network_paramaters(model_key);
    auto string1 = parameter_map->const_get("linear1");
    ASSERT_OK(string1);
    auto string2 = parameter_map->const_get("linear2");
    ASSERT_OK(string2);

    const bool both_exist = string1.has_value() && string2.has_value();

    if (both_exist) {
      std::istringstream stream1(*string1);
      std::istringstream stream2(*string2);
      torch::load(locals_.linear1, stream1);
      torch::load(locals_.linear2, stream2);
    }
    return Ok();
  }
  Void Save(const string &model_key,
            RedisInterface *redis_interface) const override {
    std::ostringstream stream1;
    std::ostringstream stream2;
    torch::save(locals_.linear1, stream1);
    torch::save(locals_.linear2, stream2);

    string string1 = stream1.str();
    string string2 = stream2.str();

    auto parameter_map = redis_interface->network_paramaters(model_key);
    ASSERT_SUCCEEDS(parameter_map->put("linear1", string1));
    ASSERT_SUCCEEDS(parameter_map->put("linear2", string2));
    return Ok();
  }
};
} // namespace impl

Unique<ForwardBackwardModel> CreateBasicModel(const NetworkSpec &model_spec) {
  auto result = new impl::BasicModel();
  auto concat_size = EmbeddingConcatSize(model_spec);
  ASSERT_EXISTS(concat_size);

  const size_t numOucomes = model_spec.output_space().dimension();
  ASSERT(*concat_size > 0);
  ASSERT(model_spec.hidden_dim() > 0);
  ASSERT(numOucomes > 0);
  result->locals_.linear1 =
      torch::nn::Linear(*concat_size, model_spec.hidden_dim());
  result->locals_.linear2 =
      torch::nn::Linear(model_spec.hidden_dim(), numOucomes);
  result->network_spec_ = model_spec;

  return result;
}

} // namespace dimensionrank
