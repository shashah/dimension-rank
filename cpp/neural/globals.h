#ifndef neural_globals_h
#define neural_globals_h 
#include "util/includes.h"

namespace dimensionrank {
namespace globals {


//// For each "embedding group", we have a map from "value" to "embedding".
//Unique<DoubleVectorMap> embedding_map(const string &embedding_group);

} // namespace dimensionrank
#endif /* neural_globals_h */
