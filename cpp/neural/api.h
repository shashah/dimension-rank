#ifndef neural_api_h
#define neural_api_h
#include "coln/api.h"
#include "data/dimensionrank.pb.h"
#include "data/globals.h"
#include "neural/features.pb.h"
#include "torch/torch.h"
#include "util/includes.h"

namespace dimensionrank {
using torch::Tensor;
using torch::nn::Module;

// Assumes Torch.
struct ForwardBundle {
  Tensor output;
  Module module_state;
};

// Converts domain-specific problem type to network.
template <typename T> class ProblemConveter {
public:
  virtual Option<DecisionRequestAndResponse>
  ConvertProblem(const T &input) const = 0;
  virtual Option<T> ConvertResponse(const DecisionResponse &input) const = 0;
};

class PredictionModel {
public:
  // Make a prediction.
  virtual Unique<DecisionResponse>
  Predict(const DecisionRequest &request,
          RedisInterface *redis_interface) const = 0;

  // Load a model from the file.
  virtual Void Load(const string &model_key,
                    RedisInterface *redis_interface) = 0;
};

class TrainableModel : public PredictionModel {
public:
  // Train on one example.
  virtual Unique<DecisionResponse>
  Train(const DecisionRequest &request, const DecisionResponse &gold_response,
        RedisInterface *redis_interface) = 0;

  // Save a model to file.
  virtual Void Save(const string &model_key,
                    RedisInterface *redis_interface) const = 0;
};

// Assumes torch, by including ForwardBundle.
class ForwardBackwardModel : public TrainableModel {
public:
  //
  // Derived functions.
  Unique<DecisionResponse>
  Predict(const DecisionRequest &request,
          RedisInterface *redis_interface) const override {
					Print_line();
    auto forward_result = Forward(request, redis_interface);
					Print_line();
    ASSERT_OK(forward_result);
					Print_line();
    return ResponseFromActivation(forward_result->output);
  }

  Unique<DecisionResponse>
  Train(const DecisionRequest &request, const DecisionResponse &gold_response,
        RedisInterface *redis_interface) override {
    // Forward.
    auto forward_bundle = Forward(request, redis_interface);
    ASSERT_OK(forward_bundle);
    // Forward.

    // Backward.
    ASSERT_SUCCEEDS(Backward(*forward_bundle, gold_response, redis_interface));
    // Forward.

    // Response.
    auto response = ResponseFromActivation(forward_bundle->output);
    ASSERT_OK(response);
    // Forward.
    return response;
  }

  //
  // Pure virtuals.

  // Prediction mode.
  virtual Unique<ForwardBundle>
  Forward(const DecisionRequest &request,
          RedisInterface *redis_interface) const = 0;

  virtual Unique<DecisionResponse>
  ResponseFromActivation(const Tensor &tensor) const = 0;

  // Training mode.
  // 1) Parameters should be updated in place.
  // 2) The predicted value should be returned for loss calculation outside.
  // 3) Sub-class should fail if it does not think 'response' is a valid output
  // for 'request'.
  virtual Void Backward(const ForwardBundle &output,
                        const DecisionResponse &gold_response,
                        RedisInterface *redis_interface) = 0;
};

DoubleVector RandomVector(size_t dimension);

} // namespace dimensionrank
#endif /* neural_api_h */
