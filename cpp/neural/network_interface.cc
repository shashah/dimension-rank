#include "neural/network_interface.h"

#include "data/globals.h"
#include "neural/basic_model.h"
#include "util/includes.h"

#include <mutex>

std::mutex g_pages_mutex;

namespace dimensionrank {

namespace {

// A 'Simple' network interface wraps a single network shared between training
// and prediction. Access to the n
class SimpleNetworkInterface : public NetworkInterface {
public:
  Const<PredictionModel> prediction_network() const override {
    std::lock_guard<std::mutex> guard(g_pages_mutex);
    ASSERT(underlying_ != nullptr);
    return underlying_.get();
  }

  Mutable<ForwardBackwardModel> training_network() override {
    std::lock_guard<std::mutex> guard(g_pages_mutex);
    ASSERT(underlying_ != nullptr);
    return underlying_.get();
  }

  Void Load(const string &model_key,
          RedisInterface *redis_interface) override {
    std::lock_guard<std::mutex> guard(g_pages_mutex);
    ASSERT_SUCCEEDS(underlying_->Load(model_key, redis_interface));
    return Ok();
  }

  Void Save(const string &model_key,
          RedisInterface *redis_interface) const override {
    std::lock_guard<std::mutex> guard(g_pages_mutex);
    ASSERT_SUCCEEDS(underlying_->Save(model_key, redis_interface));
    return Ok();
  }

public:
  unique_ptr<ForwardBackwardModel> underlying_;
};


} // namespace

Unique<NetworkInterface> LoadNetworkInterface(const NetworkModel &options,
                                              RedisInterface *redis_interface) {
	NetworkInterface *result = nullptr;

  // Step 1: Create a store.
  {
    auto model = CreateBasicModel(options.network_spec());
    ASSERT_OK(model);
    ForwardBackwardModel *u1 = model.release();
    auto subclass_store = new SimpleNetworkInterface();
    subclass_store->underlying_.reset(u1);
    result = subclass_store;
  }

  // Step 2: Load the model from the key.
	{
		ASSERT_SUCCEEDS(result->training_network()->Load(
				options.model_key(), redis_interface));
	}

  return result;
}

} // namespace dimensionrank
