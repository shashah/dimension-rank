#include "neural/store_embeddings.h"

#include "data/globals.h"

#include "util/includes.h"

namespace dimensionrank {

DoubleVector VectorFromTensor(const Tensor &tensor) {
  DoubleVector result;
  for (size_t i = 0; i < tensor.dim(); ++i) {
    float f = tensor[i].item<float>();
    result.add_weight(f);
  }
  return result;
}

namespace {

Shared<Tensor> ImplLoadEmbeddingTensor(const Feature &feature, size_t dimension,
                                       size_t depth,
                                       RedisInterface *redis_interface);

Shared<Tensor> ResetAndLoadEmbeddingTensor(const Feature &feature,
                                           size_t dimension,
                                           RedisInterface *redis_interface) {
  ASSERT_SUCCEEDS(ResetEmbedding(feature.embedding_group(), feature.value(),
                                 dimension, redis_interface));

  auto tensor = ImplLoadEmbeddingTensor(feature, dimension, 1, redis_interface);
  ASSERT_EXISTS(tensor);
  return tensor;
}

Shared<Tensor> ImplLoadEmbeddingTensor(const Feature &feature, size_t dimension,
                                       size_t depth,
                                       RedisInterface *redis_interface) {
  auto embedding_map = redis_interface->embedding_map(feature.embedding_group());
  auto embedding = embedding_map->const_get(feature.value());
  ASSERT_OK(embedding);

  if (!embedding.has_value() && depth == 0) {
    return ResetAndLoadEmbeddingTensor(feature, dimension, redis_interface);
  } else if (!embedding.has_value()) {
    // Haven't seen this case yet, but to prevent infinite recursion.
    RAISE_EXCEPTION("Too many recursions for loading the tensor.");
  }

  auto result = torch::empty({embedding->weight_size()});
  for (int i = 0; i < embedding->weight_size(); ++i) {
    result[i] = embedding->weight(i);
  }
  // TODO(greg) do we need this clone here?
  auto result1 = result.clone();
  result1.set_requires_grad(true);
  return result;
}

} // namespace

Shared<Tensor> LoadEmbeddingTensor(const Feature &feature, size_t dimension,
                                   RedisInterface *redis_interface) {
  auto r = ImplLoadEmbeddingTensor(feature, dimension, 0, redis_interface);
  ASSERT_OK(r);
  return r;
}

void SaveEmbeddingTensor(const Feature &feature, const Tensor &tensor,
                         RedisInterface *redis_interface) {
  auto embedding_map = redis_interface->embedding_map(feature.embedding_group());
  embedding_map->put(feature.value(), VectorFromTensor(tensor));
}

Void ResetEmbedding(const string &embedding_group, const string &value,
                    size_t dimension, RedisInterface *redis_interface) {
  auto embedding_map =
      redis_interface->embedding_map(embedding_group);
  DoubleVector vector;
  // TODO(greg) Do official way.
  for (size_t i = 0; i < dimension; ++i) {
    int v = rand() % 200 - 100;
    vector.add_weight(0.01 * v);
  }
  embedding_map->put(value, vector);
  return Ok();
}

Option<size_t> EmbeddingSize(const NetworkSpec &model_spec,
                             const string &embedding_group) {
  for (const auto &group : model_spec.embedding_group()) {
    if (group.name() == embedding_group) {
      return group.embedding_dim();
    }
  }
  return Null();
}

Option<size_t> EmbeddingConcatSize(const NetworkSpec &model_spec) {
  size_t result = 0;
  for (const auto &feature : model_spec.feature_spec()) {
    auto this_size = EmbeddingSize(model_spec, feature.embedding_group());
    ASSERT_EXISTS(this_size);
    result += *this_size;
  }
  return result;
}

} // namespace dimensionrank
