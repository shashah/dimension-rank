#ifndef neural_action_converter_h
#define neural_action_converter_h
#include "neural/api.h"
#include "util/includes.h"

namespace dimensionrank {

Feature CreateFeature(const string &name, const string &value,
                      const string &embedding_group);

class ActionConverter : public ProblemConveter<BroadcastLabel> {
public:
  Option<DecisionRequestAndResponse>
  ConvertProblem(const BroadcastLabel &input) const override {
    DecisionRequestAndResponse result;

    // 1. Make request.
    auto request = result.mutable_request();
    ASSERT(input.has_subjectid());
    *request->add_feature() =
        CreateFeature("subjectId", input.subjectid(), "user");
    ASSERT(input.has_authorid());
    *request->add_feature() =
        CreateFeature("authorId", input.authorid(), "user");
    ASSERT(input.has_objectid());
    *request->add_feature() =
        CreateFeature("objectId", input.objectid(), "broadcast");

		request->add_target(input.labelname());

		ClassLabel label;
		label.set_name(input.labelname());
		label.set_value(input.labelvalue());
    // 2. Make response.
    auto response = result.mutable_response();
    *response->mutable_label() = label;

    return result;
  }

  Option<BroadcastLabel>
  ConvertResponse(const DecisionResponse &response) const override {
		RAISE_EXCEPTION("not implemented");
//    BroadcastLabel action;
//    action.set_labelvalue(response.label());
//    return action;
  }
};

} // namespace dimensionrank
#endif /* neural_action_converter_h */
