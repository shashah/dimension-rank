// This file could be renamed "global network_interface". It provides a global
// access to a shared 'NetworkInterface'.
//
// This shared interface wraps a training and a prediction network. It is up to
// the sub-classes how they expose the training and prediction networks.
//
//
#ifndef neural_network_interface_h
#define neural_network_interface_h
#include "data/globals.h"
#include "neural/api.h"
#include "server/options.pb.h"
#include "util/includes.h"

namespace dimensionrank {

// A pair of: 1) prediction network and 2) training network interface.
class NetworkInterface {
public:
  // Returns a constant  prediction model.
  virtual Const<PredictionModel> prediction_network() const = 0;

  // Returns mutable access to a training model.
  virtual Mutable<ForwardBackwardModel> training_network() = 0;

  virtual Void Load(const string &model_key,
                    RedisInterface *redis_interface) = 0;
  virtual Void Save(const string &model_key,
                    RedisInterface *redis_interface) const = 0;
};

// Creates a network. This function will instantiate a redis connection, and
// check if the model is in redis. If so, it will be loaded.
Unique<NetworkInterface> LoadNetworkInterface(const NetworkModel &model_spec,
                                              RedisInterface *redis_interface);

} // namespace dimensionrank
#endif /* neural_network_interface_h */
