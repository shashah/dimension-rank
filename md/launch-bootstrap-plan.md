# Launch Bootstrap Plan

The following is the launch plan to launch the network.

## Clear the Databases
### Mongo
Assume we are using database `alpha0`.
In the shell, run:

```
use alpha0
```

To see whether there is data in there, run:

```
show collections
```

To DROP the database (which will DELETE all your old data), run:

```
db.dropDatabase()
```

To check that the database has been cleared, run:

```
show collections
```

### Redis
Enter the redis client for the server of interest.
To see current keys, type:

```
keys *
```

To flush the database, run:

```
flushall
```

## Make an Application
Go to https://deeprevelations.app, enter the root user's email, and apply.

## Approve Your Own Application
Enter the mongo client. To find the applications, run:

```
db.applications.find({})
```

From here, get the ID of your application. Then, assuming the ID is `${ID}`, run:

```
node tools/inviteUser.js --options ~/secrets/deep-revelations.node.json --applicationId ${ID}
```
